<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Guide to Iceland</title>

    <!-- Bootstrap -->

    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,600,700" rel="stylesheet">
    <link href="{{ asset('public/css/calendar.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/rateit.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/jquery.datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/lightslider.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/hover.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/media.css') }}" rel="stylesheet">
        <link href="{{url('assets/web/css/jquery.rateyo.min.css')}}" rel="stylesheet">
    <!-- {!! Html::style('assets/web/css/jquery.rateyo.min.css') !!} -->
    <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--<link href="{{ url('public/assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ url('public/assets/fontawesome/css/fontawesome-all.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ url('public/assets/css/custom_all.css') }}" rel="stylesheet">--}}
    {{--<script src="{{url('public/assets/js/custom-function.js')}}"></script>--}}
    <script src="{{url('public/assets/js/jquery-3.3.1.js')}}"></script>
    <script src="{{url('public/js/bootstrap.min.js')}}"></script>
    <style type="text/css">
        .ddslick>option{
            color: black;
        }
    </style>


    {{--<style type="text/css">--}}

        {{--.copy-footer {--}}
            {{--padding-top: 20px;--}}
            {{--padding-bottom: 20px;--}}
            {{--color: #fff;--}}
            {{--background-color: #484a4c;--}}
        {{--}--}}

        {{--.script {--}}
            {{--font-family: "Seaweed Script";--}}
            {{--color: #000;--}}
            {{--text-align: center;--}}
            {{--font-size: 40px;--}}
            {{--position: relative;--}}
            {{--margin: 0;--}}
        {{--}--}}

        {{--.script span {--}}
            {{--background-color: #fff;--}}
            {{--padding: 0 0.3em;--}}
        {{--}--}}

        {{--.script:before {--}}
            {{--content: "";--}}
            {{--display: block;--}}
            {{--position: absolute;--}}
            {{--z-index: -1;--}}
            {{--top: 50%;--}}
            {{--width: 100%;--}}
            {{--border-bottom: 3px solid black;--}}
        {{--}--}}

    {{--</style>--}}
   <style type="text/css">
.preloader{
height: 100%;
width: 100%;
position: relative;
}
    .sk-spinner.sk-spinner-wave{

width: 100%;
height: 100%;
background: whitesmoke;
position: fixed;
z-index: 99999999999999;
    }
</style>
</head>
<body>

<!-- header start -->
{{--header start--}}
@include('layouts.header')
{{--header end--}}

<script type="text/javascript">
   
         var web_url = "<?php echo url('/')?>";
 
</script>
<!-- Slider -->
<div id="preloader" style="display: none;">
      <div class="sk-spinner sk-spinner-wave">
            <img src="{{url('assets/web/img/material-loader.gif')}}">
    </div>
</div>
@yield('content')

@include('layouts.footer')

    <div class="modal fade user_modal user_login" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ url('public/images/plus.png') }}"></button>

                <div class="main_wrapper account_wrapper checkout_wrapper m-login__signin">
                 {{--<form method="post" id="login_form" action="{{url('userlogin')}}">--}}
                 <form method="post" id="login_form" action="{{url()->current()}}">
                        <div class="form-group">
                            <input type="hidden" name="route" value="{{url('home')}}">

                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                     <p id="loginpassword" style="color: red;text-align: left"></p>
                        <a href="forgot.html" class="forgot">Forgot password ?</a>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" id="m_login_signup_submit" class="btn hvr-float-shadow view_all ">Login</button>

                    @if ($errors->login->any())
                        <script type="text/javascript"> $('.user_login').modal('show');</script>
                        <div class="alert alert-danger">
                            <ul>
                                    @foreach($errors->login->toArray() as $err)
                                    <li style="text-align:center">{{ $err[0] }}</li><hr/>
                                    @endforeach

                            </ul>
                        </div>
                    @endif
                    </form>

                </div>
                <div class="booking_summary border-bottom">
                    <h4>Login with</h4>
                </div>
                <div class="login_with">
                    <a href="#" class="login_fb hvr-float-shadow">
                        <span>Login with Facebook</span>
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#" class="login_google hvr-float-shadow">
                        <span>Login with Google</span>
                        <i class="fab fa-google"></i>
                    </a>
                </div>

            </div>

        </div>
    </div>

<!-- <div class="modal fade user_modal user_login" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="{{ url('public/images/plus.png') }}"></button>
            <div class="row">
                <div class="col-6 border-right">
                    <div class="main_wrapper account_wrapper">
                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <a href="forgot.html" class="forgot">Forgot password ?</a>
                            <button type="submit" class="btn hvr-float-shadow view_all">Login</button>
                        </form>
                    </div>

                </div>
                <div class="col-6">
                    <div class="booking_summary border-bottom">
                        <h4>Login with</h4>
                    </div>
                    <div class="login_with">
                        <a href="#" class="login_fb hvr-float-shadow">
                            <span>Login with Facebook</span>
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#" class="login_google hvr-float-shadow">
                            <span>Login with Google</span>
                            <i class="fab fa-google"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- <div class="modal fade user_modal user_signup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="{{ url('public/images/plus.png') }}"></button>
            <div class="row">
                <div class="col-6 border-right">
                    <div class="main_wrapper account_wrapper">
                        <form>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" id="inputAddress" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" id="inputAddress" placeholder="Last name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                                </div>
                                <div class="form-group col-12">
                                    <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputAddress" placeholder="Addresss">
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="City" id="inputCity">
                                </div>
                                <div class="form-group col-md-4">
                                    <select id="inputState" class="selectpicker">
                                        <option selected>Choose...</option>
                                        <option>option 1</option>
                                        <option>option 2</option>
                                        <option>option 3</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" id="inputZip" placeholder="Zip">
                                </div>
                            </div>
                            <button type="submit" class="btn hvr-float-shadow view_all">Register</button>
                        </form>
                    </div>
                </div>
                <div class="col-6">
                    <div class="booking_summary border-bottom">
                        <h4>Login with</h4>
                    </div>
                    <div class="login_with">
                        <a href="#" class="login_fb hvr-float-shadow">
                            <span>Login with Facebook</span>
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#" class="login_google hvr-float-shadow">
                            <span>Login with Google</span>
                            <i class="fab fa-google"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

    <div class="modal fade user_modal user_signup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ url('public/images/plus.png') }}"></button>

                <div class="main_wrapper account_wrapper checkout_wrapper">
                   
                    {{--<form method="post" id="signup_form" action="{{url('user_registration')}}">--}}
                    <form method="post" id="signup_form" >
                        <p id="errorFields" style="color: red;text-align: center"></p>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="su_first_name"  name="first_name" placeholder="First name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                           <input type="email" class="form-control " id="register_email" name="email" placeholder="Email">
                           <span id="registerEmail" style="color: #c20904;text-align: left"></span>
                            </div>
                            <div class="form-group col-6">
                                <input type="password" class="form-control" name="password" id="inputPassword4" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Addresss">
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" name="city" class="form-control" placeholder="City" id="inputCity">
                            </div>
                            <div class="form-group col-md-6">
                                {{--<select id="inputState" name="country_code" class="selectpicker">--}}
                                <select id="inputState" name="country_code" class="selectpicker">
                                    <option >Select Country</option>
                                    <?php
                                    $country=  \App\Models\Country ::all()
                                    ?>
                                    @if(!empty($country))
                                        @foreach($country as $obj)
                                            <option name="{{$obj->code}}">{{$obj->name}}</option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                            {{--<div class="form-group col-md-2">--}}
                                {{--<input type="text" name="zip" class="form-control" id="inputZip" placeholder="Zip">--}}
                            {{--</div>--}}
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="javascript:void(0);"  id="m_register_signup_submit" class="btn hvr-float-shadow view_all">Register</a>
                         @if ($errors->register->any())
                    <script type="text/javascript"> $('.user_signup').modal('show');</script>
                    <div class="alert alert-danger">
                        <ul>
                                @foreach($errors->register->toArray() as $err)
                                    <li style="text-align:center">{{ $err[0] }}</li><hr/>
                                @endforeach
                            
                        </ul>
                    </div>
                    @endif
                    </form>
                </div>

                <div class="booking_summary border-bottom">
                    <h4>Login with</h4>
                </div>
                <div class="login_with">
                    <a href="#" class="login_fb hvr-float-shadow">
                        <span>Login with Facebook</span>
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#" class="login_google hvr-float-shadow">
                        <span>Login with Google</span>
                        <i class="fab fa-google"></i>
                    </a>
                </div>

            </div>
        </div>
    </div>


{{--<script src="{{url('public/js/jquery-3.2.1.min.js')}}"></script>--}}

<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('public/js/jquery.datetimepicker.full.min.js')}}"></script>
<script src="{{asset('public/js/slick.min.js')}}"></script>
<script src="{{asset('public/js/wow.min.js')}}"></script>
<script src="{{asset('public/js/lightslider.js')}}"></script>
<script src="{{asset('public/js/jquery.rateit.min.js')}}"></script>
<script src="{{asset('public/js/custom.js')}}"></script>

<script  src="https://openexchangerates.github.io/money.js/money.min.js"></script>
<script src="{{asset('public/js/currency_conversion.js')}}"></script>
<script src="{{asset('public/js/jquery.validate.min.js')}}"></script>
{{--<script src="{{url('public/assets/js/jquery-3.3.1.js')}}"></script>--}}
{{--<script src="{{url('public/assets/bootstrap/js/bootstrap.js')}}"></script>--}}
{{--<!-- <script src="{{url('public/assets/js/custom-function.js')}}"></script> -->--}}
{{-- <script src="http://malsup.github.com/jquery.form.js')}}"></script> --}}
{{--<script src="{{url('public/assets/js/login.js')}}"></script>--}}
<script src="{{url('assets/web/js/jquery.rateyo.min.js')}}"></script>
  <!-- {!! Html::script('assets/web/js/jquery.rateyo.min.js') !!} -->

<!-- auto -->

{{--<script src="{{asset('assets/web/js/user/login.js')}}"></script>--}}
<script>
    $(document).ready(function () {
        $('#m_login_signup_submit').click(function (e) {

            var token = $("input[name='_token']").val();
            var email=$('#exampleInputEmail1').val();
            var password=$('#exampleInputPassword1').val();
            //alert(email)
            $.ajax
            ({
                type: "POST",
                url: web_url + "/loginVerfi",
                data: {'email': email,'password': password, '_token': token},
                success: function (data) {
                    if(data !=''){
                        $("#loginpassword").html(data);
                    }else{

                        $("#loginpassword").html('');
                       /* ajaxSent = true;
                        $('form').submit();
                        return true;*/
                        location.reload();  //

                    }

                }
            });
        });
        $('#m_register_signup_submit').click(function (e) {

            var token = $("input[name='_token']").val();

            var first_name = $("#su_first_name").val();
            var last_name = $("#last_name").val();
            var register_email = $("#register_email").val();
            var password = $("#inputPassword4").val();
            var address = $("#inputAddress").val();
            var city = $("#inputCity").val();
            var state = $("#inputState").val();
            var zip = $("#inputZip").val();
            //alert(register_email);
            if (first_name == '' || last_name == '' || zip == '' || register_email == '' || password == '' || city == '')
            {
                 $('#errorFields').html("Please fill all fields...!!!!!!");

            }else {

                // var email=$('#register_email').val();
                // var data = $("#signup_form").serialize();
                // var password=$('#exampleInputPassword1').val();
                //alert(email)
                $.ajax
                ({
                    type: "POST",
                    url: web_url + "/check-email-register",
                    data: {'first_name': first_name,
                        'last_name': last_name,
                        'email': register_email,
                        'password': password,
                        'address': address,
                        'city': city,
                        'state': state,
                        'zip': zip,
                        '_token': token},
                    success: function (data) {
                        if(data !=''){
                            $("#registerEmail").html(data);
                        }else{

                            // $("#registerEmail").html('');
                            /* ajaxSent = true;
                             $('form').submit();
                             return true;*/
                            location.reload();  //

                        }

                    }
                });
            }




        })
    })
</script>

</body>
</html>

<script type="text/javascript">

    $("#login_form").validate({submitHandler: function(form) {
            // do other things for a valid form
            // form.submit();
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            password:{
                required:true,
                minlength:6
            }
        },
        messages: {
            email: {
                required: "We need your email address to log you in",
                email: "Your email address must be in the format of name@domain.com"
            },
            password: {
                required: "Password is required",
                minlength: "Your password must contain minimum 6 characters"
            }
        }
    });
    $("#signup_form").validate({
        submitHandler: function(form) {
           // alert('sdfas');
            // do other things for a valid form
            // form.submit();
        },
        rules: {
            country_code: "required",
            zip: "required",
            address: "required",
            city: "required",
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            password:{
                required:true,
                minlength:6
            }
        },
        messages: {
            first_name: "Please specify your first name",
            address: "Please specify your Address",
            country_code: "Please Choose your Country",
            city: "Please specify your City",
            zip: "Required",
            last_name: "Please specify your last name",
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            password: {
                required: "Password is required",
                minlength: "Your password must contain minimum 6 characters"
            }
        }
    });

</script>


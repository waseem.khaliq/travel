<header>
    <nav class="custom_navbar navbar fixed-top navbar-expand-lg">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{url('public/images/logo.png')}}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">
					<i class="fas fa-bars"></i>
				</span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Book your Adventures
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Day Tours</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Northern Lights</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Discover Iceland</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        All tours
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Self-Drive Packages</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Blue lagoon</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Golden Circle</a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Super Jeep</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Snow Mobile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Private  Tours</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Offers</a>

                    </div>
                </li>
            </ul>

        </div>
        <ul class="account_cart_links my-2 my-lg-0">
            @if(empty(Auth::id()))

                <li>
                    <a href="#" data-toggle="modal" data-target=".user_login">Login</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target=".user_signup">
                        <span>|</span>Signup
                    </a>
                </li>
            @endif
            <li>
                <a href="#">
                    <i class="fas fa-shopping-cart"></i>
                </a>
            </li>
            <li>
                <a href="{{url('search')}}">
                    <i class="fas fa-search"></i>
                </a>
            </li>
            <li class="nav-item dropdown choose_language">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{url('public/images/flags_03.png')}}">
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_03.png')}}">
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_05.png')}}">
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_08.png')}}">
                    </a>
                </div>
            </li>
            @if(!empty(Auth::id()))
                <li>
                    <a href="#" class="user">
                        <img src="{{url('public/images/0.jpg')}}">
                    </a>
                    <div class="edit_user">
                        <ul>
                            <li><a href="{{url('edit-profile')}}">Edit Profile</a></li>
                            <li><a href="{{url('change-password')}}">Change Password</a></li>
                            <li><a href="{{url('logout')}}">Logout</a></li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </nav>
</header>
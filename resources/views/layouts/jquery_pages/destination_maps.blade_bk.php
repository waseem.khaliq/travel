<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPUTGhVxI4piPZBg8wXT587e9EzDOar5w">
</script>
<script>
//  $('input').iCheck({
//        checkboxClass: 'icheckbox_square-grey',
//      radioClass: 'iradio_square-grey'
//  });
    ///////rat yo plugin listing goes here////////////////////
    $(function () {
        $(".rateYo-sidebar-5").rateYo({
            rating: 5,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-4").rateYo({
            rating: 4,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-3").rateYo({
            rating: 3,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-2").rateYo({
            rating: 2,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-1").rateYo({
            rating: 1,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
    });

    function showRating(i, rating) {
        $(".rateYo-" + i).rateYo({
            rating: rating,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
    }

</script>



<script>
    $(document).ready(function() {
        $(document).on('click', '#overview', function () {
            //alert(<?php echo $city_map;?>);
            CityInitMap();
        });

        function CityInitMap() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the web page
            map = new google.maps.Map(document.getElementById("map_city"), mapOptions);
            map.setTilt(50);

            //console.log($city_marker);

            // Multiple markers location, latitude, and longitude
            var markers = '<?php echo $city_map;?>';

            // Info window content
            var infoWindowContent = '<?php echo $city_marker;?>';
            // Add multiple markers to map
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            // Place each marker on the map
            var icon = {
                url: "<?php echo url('/assets/web/img/map_marker.png');?>",
                // scaledSize: new google.maps.Size(65, 60)
                //labelOrigin: new google.maps.Point(5, 0)
                // scaledSize: new google.maps.Size(width, height), // size
                //origin: new google.maps.Point(0,0), // origin
                ///anchor: new google.maps.Point(anchor_left, anchor_top) // anchor
            };

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    //title: markers[i][0],
                    icon: icon

                });

                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));

                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
            }


            // Set zoom level
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(6);
                google.maps.event.removeListener(boundsListener);
            });

        }
    });
</script>

<script>
//  $('#sort').change(function () {
//        if ($(this).val() != '') {
//            var display_id = $(this).val();
//            window.location = web_url + "/hotels?sort=" + $(this).val();
//        }
//    });
//    //$('.stars').change(function () {
//    $('#check_box :checked').each(function() {
//        alert($(this).val());
//        //allVals.push($(this).val());
//    });
//    $(document).on('click', '.stars', function() {
//        alert('sadfd');
//    });
       //////////////////////////////////Map view...///////////////////////////

$(document).ready(function() {
    $(document).on('click', '#hotels', function () {
        //alert(<?php echo $hotels_map_list;?>);
        HotelsInitMap();
    });

    function HotelsInitMap() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the web page
        map = new google.maps.Map(document.getElementById("map_hotel"), mapOptions);
        map.setTilt(50);

        // Multiple markers location, latitude, and longitude
        var markers = '<?php echo $hotels_map_list;?>';

        // Info window content
        var infoWindowContent = '<?php echo $hotels_marker_list;?>';
        // Add multiple markers to map
        var infoWindow = new google.maps.InfoWindow(), marker, i;
        // Place each marker on the map
        var icon = {
            url: "<?php echo url('/assets/web/img/map_marker.png');?>",
           // scaledSize: new google.maps.Size(65, 60)
            //labelOrigin: new google.maps.Point(5, 0)
            // scaledSize: new google.maps.Size(width, height), // size
            //origin: new google.maps.Point(0,0), // origin
            ///anchor: new google.maps.Point(anchor_left, anchor_top) // anchor
        };

        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0],
                //labelContent:'<div style="text-align:center;"><b>'+markers[i][3]+'</b></div>',
                label: {
                    text: markers[i][3],
                    color: 'white'
                },
                icon: icon

            });

            // Add info window to marker
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Center the map to fit all markers on the screen
            map.fitBounds(bounds);
        }


        // Set zoom level
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
            this.setZoom(12);
            google.maps.event.removeListener(boundsListener);
        });

    }
});

</script>

<script>
    //////////////////////////////////Places Map view...///////////////////////////
    $(document).ready(function() {
        $(document).on('click','#places',function () {
            placesinitMap();
        })

        function placesinitMap() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the web page
            map = new google.maps.Map(document.getElementById("map_places"), mapOptions);
            map.setTilt(50);

            // Multiple markers location, latitude, and longitude
            var markers =   <?php echo $places_map_list;?>;

            // Info window content
            var infoWindowContent =<?php echo $places_marker_list;?>;
            // Add multiple markers to map
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            // Place each marker on the map
            var icon = {
                url: "{{url('/')}}/assets/web/img/map_marker.png", // url
                scaledSize: new google.maps.Size(65, 60)
                //labelOrigin: new google.maps.Point(5, 0)
                // scaledSize: new google.maps.Size(width, height), // size
                //origin: new google.maps.Point(0,0), // origin
                ///anchor: new google.maps.Point(anchor_left, anchor_top) // anchor
            };

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0],
                    icon: icon

                });
                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
            }
            // Set zoom level
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(12);
                google.maps.event.removeListener(boundsListener);
            });

        }

    });


</script>

<script>
    //////////////////////////////////Places Map view...///////////////////////////
    $(document).ready(function() {
        $(document).on('click','#activities',function () {
            activitiesinitMap();
        })

        function activitiesinitMap() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the web page
            map = new google.maps.Map(document.getElementById("map_activities"), mapOptions);
            map.setTilt(50);

            // Multiple markers location, latitude, and longitude
            var markers = '<?php echo $activity_map_list;?>';

            // Info window content
            var infoWindowContent ='<?php echo $activity_marker_list;?>';
            // Add multiple markers to map
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            // Place each marker on the map
            var icon = {
                url: "{{url('/')}}/assets/web/img/map_marker.png", // url
                scaledSize: new google.maps.Size(65, 60)
                //labelOrigin: new google.maps.Point(5, 0)
                // scaledSize: new google.maps.Size(width, height), // size
                //origin: new google.maps.Point(0,0), // origin
                ///anchor: new google.maps.Point(anchor_left, anchor_top) // anchor
            };

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0],
                    icon: icon

                });
                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
            }
            // Set zoom level
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(12);
                google.maps.event.removeListener(boundsListener);
            });

        }

    });


</script>

<script>

    /////////////REstaurants map view////

    $(document).ready(function() {
        $(document).on('click','#restaurant',function () {
            //alert(<?php echo $restaurants_map_list;?>);
            restInitMap();
        })

        function restInitMap() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the web page
            map = new google.maps.Map(document.getElementById("map_restaurants"), mapOptions);
            map.setTilt(50);

            // Multiple markers location, latitude, and longitude

            var markers = <?php echo $restaurants_map_list;?>;
            // Info window content
            var infoWindowContent =<?php echo $restaurants_marker_list;?>;
            // Add multiple markers to map
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            // Place each marker on the map
            var icon = {
                url: "{{url('/')}}/assets/web/img/map_marker.png", // url
                scaledSize: new google.maps.Size(65, 60)
                //labelOrigin: new google.maps.Point(5, 0)
                // scaledSize: new google.maps.Size(width, height), // size
                //origin: new google.maps.Point(0,0), // origin
                ///anchor: new google.maps.Point(anchor_left, anchor_top) // anchor
            };

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0],
                    icon: icon

                });
                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
            }
            // Set zoom level
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom(12);
                google.maps.event.removeListener(boundsListener);
            });

        }

    });


</script>


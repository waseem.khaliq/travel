<script>
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey'
    });
    ///////rat yo plugin listing goes here////////////////////
    $(function () {
        $(".rateYo-sidebar-5").rateYo({
            rating: 5,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-4").rateYo({
            rating: 4,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-3").rateYo({
            rating: 3,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-2").rateYo({
            rating: 2,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
        $(".rateYo-sidebar-1").rateYo({
            rating: 1,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
    });


    function showRating(i, rating) {
        $(".rateYo-" + i).rateYo({
            rating: rating,
            spacing: "5px",
            readOnly: true,
            starWidth: "20px"
        });
    } ;
</script>
<script>
    $('#sort').change(function () {
        if ($(this).val() != '') {
            var display_id = $(this).val();
            window.location = web_url + "/hotels?sort=" + $(this).val();
        }
    });
    //$('.stars').change(function () {
    $('#check_box :checked').each(function() {
        alert($(this).val());
        //allVals.push($(this).val());
    });
    $(document).on('click', '.stars', function() {
        alert('sadfd');
    });
</script>
{{--over view maps --}}
<script>



    (function(A) {
        if (!Array.prototype.forEach)
            A.forEach = A.forEach || function(action, that) {
                for (var i = 0, l = this.length; i < l; i++)
                    if (i in this)
                        action.call(that, this[i], i, this);
            };

    })(Array.prototype);

    var
        mapObject,
        markers = [],
        markersData = {'Hotels':<?php if(!empty($hotels_map_list)){echo $hotels_map_list; } ?>}

    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(<?php if(!empty($lat)){echo $lat; }?>, <?php if(!empty($long)){echo $long; } ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        styles: [
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "hue": "#FFBB00"
                    },
                    {
                        "saturation": 43.400000000000006
                    },
                    {
                        "lightness": 37.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers": [
                    {
                        "hue": "#FFC200"
                    },
                    {
                        "saturation": -61.8
                    },
                    {
                        "lightness": 45.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51.19999999999999
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 52
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "hue": "#0078FF"
                    },
                    {
                        "saturation": -13.200000000000003
                    },
                    {
                        "lightness": 2.4000000000000057
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "hue": "#00FF6A"
                    },
                    {
                        "saturation": -1.0989010989011234
                    },
                    {
                        "lightness": 11.200000000000017
                    },
                    {
                        "gamma": 1
                    }
                ]
            }
        ]
    };



    var marker;
    mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
    for (var key in markersData)

        markersData[key].forEach(function (item) {

            //alert(item.get_directions_start_address);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                map: mapObject,

                icon: item.get_directions_start_address,
            });

            if ('undefined' === typeof markers[key])
                markers[key] = [];
            markers[key].push(marker);
            google.maps.event.addListener(marker, 'mouseover', (function () {
                closeInfoBox();
                getInfoBox(item).open(mapObject, this);
                /* mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));*/
            }));


        });

    function hideAllMarkers () {
        for (var key in markers)
            markers[key].forEach(function (marker) {
                marker.setMap(null);
            });
    };

    function toggleMarkers (category) {
        hideAllMarkers();
        closeInfoBox();

        if ('undefined' === typeof markers[category])
            return false;
        markers[category].forEach(function (marker) {
            marker.setMap(mapObject);
            marker.setAnimation(google.maps.Animation.DROP);

        });
    };

    function closeInfoBox() {
        $('div.infoBox').remove();
    };


    function getInfoBox(item) {

        return new InfoBox({
            content:
            '<div class="marker_info_2">' +
            '<img width="240px" height="150px" src="' + item.map_image_url + '" alt="Image"/>' +
            '<h3>'+ item.name_point +'</h3>' +
            '<span>'+ item.description_point +'</span>' +
            '<div class="marker_tools">' +
            '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block"">' +
            '<input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden">' +
            '<input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'">' +
            '<button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
            '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
            '</div>' +
            '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
            '</div>',
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(10, 125),
            closeBoxMargin: '1px -14px 2px 2px',
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            isHidden: false,
            alignBottom: true,
            pane: 'floatPane',
            enableEventPropagation: true
        });

    };

    function onHtmlClick(location_type, key){
        /*alert(markers[location_type][key]);*/
        google.maps.event.trigger(markers[location_type][key], "click");
    }




</script>
{{--hotel maps view --}}
<script>
    $(document).ready(function(){
        $(document).on("click","#hotels", function(){

    (function(A) {
        if (!Array.prototype.forEach)
            A.forEach = A.forEach || function(action, that) {
                for (var i = 0, l = this.length; i < l; i++)
                    if (i in this)
                        action.call(that, this[i], i, this);
            };

    })(Array.prototype);

    var
        mapObject,
        markers = [],
        markersData = {'Hotels':<?php if(!empty($hotels_map_list)){echo $hotels_map_list; } ?>}

    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(<?php if(!empty($lat)){echo $lat; }?>, <?php if(!empty($long)){echo $long; } ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        styles: [
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "hue": "#FFBB00"
                    },
                    {
                        "saturation": 43.400000000000006
                    },
                    {
                        "lightness": 37.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers": [
                    {
                        "hue": "#FFC200"
                    },
                    {
                        "saturation": -61.8
                    },
                    {
                        "lightness": 45.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51.19999999999999
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 52
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "hue": "#0078FF"
                    },
                    {
                        "saturation": -13.200000000000003
                    },
                    {
                        "lightness": 2.4000000000000057
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "hue": "#00FF6A"
                    },
                    {
                        "saturation": -1.0989010989011234
                    },
                    {
                        "lightness": 11.200000000000017
                    },
                    {
                        "gamma": 1
                    }
                ]
            }
        ]
    };
    var marker;
    mapObject = new google.maps.Map(document.getElementById('map_hotel'), mapOptions);
    for (var key in markersData)

        markersData[key].forEach(function (item) {

            //alert(item.location_latitude);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                map: mapObject,

                icon: item.get_directions_start_address,
            });

            if ('undefined' === typeof markers[key])
                markers[key] = [];
            markers[key].push(marker);
            google.maps.event.addListener(marker, 'mouseover', (function () {
                closeInfoBox();
                getInfoBox(item).open(mapObject, this);
                /* mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));*/
            }));


        });

    function hideAllMarkers () {
        for (var key in markers)
            markers[key].forEach(function (marker) {
                marker.setMap(null);
            });
    };

    function toggleMarkers (category) {
        hideAllMarkers();
        closeInfoBox();

        if ('undefined' === typeof markers[category])
            return false;
        markers[category].forEach(function (marker) {
            marker.setMap(mapObject);
            marker.setAnimation(google.maps.Animation.DROP);

        });
    };

    function closeInfoBox() {
        $('div.infoBox').remove();
    };


    function getInfoBox(item) {

        return new InfoBox({
            content:
            '<div class="marker_info_2">' +
            '<img width="240px" height="150px" src="' + item.map_image_url + '" alt="Image"/>' +
            '<h3>'+ item.name_point +'</h3>' +
            '<span>'+ item.description_point +'</span>' +
            '<div class="marker_tools">' +
            '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block"">' +
            '<input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden">' +
            '<input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'">' +
            '<button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
            '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
            '</div>' +
            '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
            '</div>',
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(10, 125),
            closeBoxMargin: '1px -14px 2px 2px',
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            isHidden: false,
            alignBottom: true,
            pane: 'floatPane',
            enableEventPropagation: true
        });

    };

    function onHtmlClick(location_type, key){
        /*alert(markers[location_type][key]);*/
        google.maps.event.trigger(markers[location_type][key], "click");
    }

        });
    });

</script>
{{--places--}}
<script>
    $(document).ready(function(){
        $(document).on("click","#places", function(){

            (function(A) {
                if (!Array.prototype.forEach)
                    A.forEach = A.forEach || function(action, that) {
                        for (var i = 0, l = this.length; i < l; i++)
                            if (i in this)
                                action.call(that, this[i], i, this);
                    };

            })(Array.prototype);

            var
                mapObject,
                markers = [],
                markersData = {'Hotels':<?php if(!empty($places_map_list)){echo $places_map_list; } ?>}

            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng(<?php if(!empty($places_lat)){echo $places_lat; }?>, <?php if(!empty($places_long)){echo $places_long; } ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                styles: [
                    {
                        "featureType": "landscape",
                        "stylers": [
                            {
                                "hue": "#FFBB00"
                            },
                            {
                                "saturation": 43.400000000000006
                            },
                            {
                                "lightness": 37.599999999999994
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "stylers": [
                            {
                                "hue": "#FFC200"
                            },
                            {
                                "saturation": -61.8
                            },
                            {
                                "lightness": 45.599999999999994
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "stylers": [
                            {
                                "hue": "#FF0300"
                            },
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 51.19999999999999
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "stylers": [
                            {
                                "hue": "#FF0300"
                            },
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 52
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {
                                "hue": "#0078FF"
                            },
                            {
                                "saturation": -13.200000000000003
                            },
                            {
                                "lightness": 2.4000000000000057
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "hue": "#00FF6A"
                            },
                            {
                                "saturation": -1.0989010989011234
                            },
                            {
                                "lightness": 11.200000000000017
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    }
                ]
            };
            var marker;
            mapObject = new google.maps.Map(document.getElementById('map_places'), mapOptions);
            for (var key in markersData)

                markersData[key].forEach(function (item) {

                    //alert(item.location_latitude);
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                        map: mapObject,

                        icon: item.get_directions_start_address,
                    });

                    if ('undefined' === typeof markers[key])
                        markers[key] = [];
                    markers[key].push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function () {
                        closeInfoBox();
                        getInfoBox(item).open(mapObject, this);
                        /* mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));*/
                    }));


                });

            function hideAllMarkers () {
                for (var key in markers)
                    markers[key].forEach(function (marker) {
                        marker.setMap(null);
                    });
            };

            function toggleMarkers (category) {
                hideAllMarkers();
                closeInfoBox();

                if ('undefined' === typeof markers[category])
                    return false;
                markers[category].forEach(function (marker) {
                    marker.setMap(mapObject);
                    marker.setAnimation(google.maps.Animation.DROP);

                });
            };

            function closeInfoBox() {
                $('div.infoBox').remove();
            };


            function getInfoBox(item) {

                return new InfoBox({
                    content:
                    '<div class="marker_info_2">' +
                    '<img width="240px" height="150px" src="' + item.map_image_url + '" alt="Image"/>' +
                    '<h3>'+ item.name_point +'</h3>' +
                    '<span>'+ item.description_point +'</span>' +
                    '<div class="marker_tools">' +
                    '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block"">' +
                    '<input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden">' +
                    '<input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'">' +
                    '<button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
                    '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                    '</div>',
                    disableAutoPan: false,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(10, 125),
                    closeBoxMargin: '1px -14px 2px 2px',
                    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                    isHidden: false,
                    alignBottom: true,
                    pane: 'floatPane',
                    enableEventPropagation: true
                });

            };

            function onHtmlClick(location_type, key){
                /*alert(markers[location_type][key]);*/
                google.maps.event.trigger(markers[location_type][key], "click");
            }

        });
    });

</script>
{{--restaurants--}}
<script>
    $(document).ready(function(){
        $(document).on("click","#restaurant", function(){

            (function(A) {
                if (!Array.prototype.forEach)
                    A.forEach = A.forEach || function(action, that) {
                        for (var i = 0, l = this.length; i < l; i++)
                            if (i in this)
                                action.call(that, this[i], i, this);
                    };

            })(Array.prototype);

            var
                mapObject,
                markers = [],
                markersData = {'Hotels':<?php if(!empty($restaurants_map_list)){echo $restaurants_map_list; } ?>}

            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng(<?php if(!empty($restaurant_lat)){echo $restaurant_lat; }?>, <?php if(!empty($restaurant_long)){echo $restaurant_long; } ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                styles: [
                    {
                        "featureType": "landscape",
                        "stylers": [
                            {
                                "hue": "#FFBB00"
                            },
                            {
                                "saturation": 43.400000000000006
                            },
                            {
                                "lightness": 37.599999999999994
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "stylers": [
                            {
                                "hue": "#FFC200"
                            },
                            {
                                "saturation": -61.8
                            },
                            {
                                "lightness": 45.599999999999994
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "stylers": [
                            {
                                "hue": "#FF0300"
                            },
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 51.19999999999999
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "stylers": [
                            {
                                "hue": "#FF0300"
                            },
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 52
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {
                                "hue": "#0078FF"
                            },
                            {
                                "saturation": -13.200000000000003
                            },
                            {
                                "lightness": 2.4000000000000057
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "hue": "#00FF6A"
                            },
                            {
                                "saturation": -1.0989010989011234
                            },
                            {
                                "lightness": 11.200000000000017
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    }
                ]
            };
            var marker;
            mapObject = new google.maps.Map(document.getElementById('map_restaurants'), mapOptions);
            for (var key in markersData)

                markersData[key].forEach(function (item) {

                    //alert(item.location_latitude);
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                        map: mapObject,

                        icon: item.get_directions_start_address,
                    });

                    if ('undefined' === typeof markers[key])
                        markers[key] = [];
                    markers[key].push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function () {
                        closeInfoBox();
                        getInfoBox(item).open(mapObject, this);
                        /* mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));*/
                    }));


                });

            function hideAllMarkers () {
                for (var key in markers)
                    markers[key].forEach(function (marker) {
                        marker.setMap(null);
                    });
            };

            function toggleMarkers (category) {
                hideAllMarkers();
                closeInfoBox();

                if ('undefined' === typeof markers[category])
                    return false;
                markers[category].forEach(function (marker) {
                    marker.setMap(mapObject);
                    marker.setAnimation(google.maps.Animation.DROP);

                });
            };

            function closeInfoBox() {
                $('div.infoBox').remove();
            };


            function getInfoBox(item) {

                return new InfoBox({
                    content:
                    '<div class="marker_info_2">' +
                    '<img width="240px" height="150px" src="' + item.map_image_url + '" alt="Image"/>' +
                    '<h3>'+ item.name_point +'</h3>' +
                    '<span>'+ item.description_point +'</span>' +
                    '<div class="marker_tools">' +
                    '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block"">' +
                    '<input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden">' +
                    '<input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'">' +
                    '<button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
                    '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                    '</div>',
                    disableAutoPan: false,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(10, 125),
                    closeBoxMargin: '1px -14px 2px 2px',
                    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                    isHidden: false,
                    alignBottom: true,
                    pane: 'floatPane',
                    enableEventPropagation: true
                });

            };

            function onHtmlClick(location_type, key){
                /*alert(markers[location_type][key]);*/
                google.maps.event.trigger(markers[location_type][key], "click");
            }

        });
    });

</script>
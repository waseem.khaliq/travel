<style>
    .dropdown-menu.show{
        min-height: 100px;
        max-height: 20em;
        overflow-y: scroll;
    }
</style>
<header>
    <nav class="custom_navbar navbar fixed-top navbar-expand-lg">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{url('public/images/logo.png')}}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">
					<i class="fas fa-bars"></i>
				</span>
        </button>

@php
$ActiviySubcat = \App\Models\Category::select('id','cat_name','slug','order_no')->where('parent_id',3)->orderBy('order_no')->get();
//dd($ActiviySubcat);
@endphp
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active submenu columnl1">
                    <a class="nav-link dropdown-toggle" href="#">Book your Adventures
                        <span class="sr-only">(current)</span>
                    </a>
                    <div class="subMenu_wrapper">

                        <ul>
                            {{--<h3>By Category</h3>--}}
                            @for($i=0; $i<8; $i++)
                            <li>
                                <a href="{{url('get-activities/'.$ActiviySubcat[$i]->slug)}}" class="hvr-underline-from-left">{{$ActiviySubcat[$i]->cat_name}}</a>
                            </li>
                            @endfor
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">Day tours</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">Romance</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">family holidays</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">best of island</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">active holidays</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">highlands</a>--}}
                            {{--</li>--}}
                        </ul>
                        <ul>
                            {{--<h3>By lenght</h3>--}}
                            @for($i=8; $i<16; $i++)
                                <li>
                                    <a href="{{url('get-activities/'.$ActiviySubcat[$i]->slug)}}" class="hvr-underline-from-left">{{$ActiviySubcat[$i]->cat_name}}</a>
                                </li>
                            @endfor
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">All tours</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">1 day</a><a href="#" class="hvr-underline-from-left">2 days</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">3 days</a><a href="#" class="hvr-underline-from-left">4 days</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">5 days</a><a href="#" class="hvr-underline-from-left">6 days</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">7 days</a><a href="#" class="hvr-underline-from-left">8 days</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">9 days</a><a href="#" class="hvr-underline-from-left">10 days</a>--}}
                            {{--</li>--}}
                        </ul>
                        <ul>
                            <h3>On Request</h3>
                            @for($i=16; $i<22; $i++)
                                <li>
                                    <a href="{{url('get-activities/'.$ActiviySubcat[$i]->slug)}}" class="hvr-underline-from-left">{{$ActiviySubcat[$i]->cat_name}}</a>
                                </li>
                            @endfor
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">All tours</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">available all year</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">only in summer</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">only in winter</a>--}}
                            {{--</li>--}}
                        </ul>
                        {{--<ul>--}}
                            {{--<h3>Requests</h3>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">Request a Private Guided Tour</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">Request help booking a trip</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    </div>
                </li>
                <li class="nav-item submenu column1">
                    <a class="nav-link dropdown-toggle" href="{{url('get-activities/day-tours')}}">Day Tours</a>
                    {{--<div class="subMenu_wrapper">--}}
                        {{--<ul>--}}
                            {{--<li class="submenu level2">--}}
                                {{--<a href="#" class="dropdown-toggle">link 1</a>--}}
                                {{--<div class="subMenu_wrapper">--}}
                                    {{--<ul>--}}
                                        {{--<li>--}}
                                            {{--<a href="#" class="hvr-underline-from-left">link 1-1</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#" class="hvr-underline-from-left">link 2-1</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#" class="hvr-underline-from-left">link 3-1</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--@for($i=24; $i<32; $i++)--}}
                                {{--<li>--}}
                                    {{--<a href="{{$ActiviySubcat[$i]->id}}" class="hvr-underline-from-left">{{$ActiviySubcat[$i]->cat_name}}</a>--}}
                                {{--</li>--}}
                            {{--@endfor--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">link 3</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">link 4</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#" class="hvr-underline-from-left">link 5</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
                <li class="nav-item submenu column1">
                    {{--<a class="nav-link" href="#">Northern Lights</a>--}}
                    <a class="nav-link dropdown-toggle" href="{{url('get-activities/northern-lights')}}">Northern Lights</a>
                    {{--<div class="subMenu_wrapper">--}}
                        {{--<ul>--}}
                            {{--@for($i=32; $i<40; $i++)--}}
                                {{--<li>--}}
                                    {{--<a href="{{url('get-activities/'.$ActiviySubcat[$i]->slug)}}" class="hvr-underline-from-left">{{$ActiviySubcat[$i]->cat_name}}</a>--}}
                                {{--</li>--}}
                            {{--@endfor--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>

<<<<<<< HEAD
                <li class="nav-item submenu column1">
                    {{--<a class="nav-link" href="#">Northern Lights</a>--}}
                    <a class="nav-link dropdown-toggle" href="#">Discover Iceland</a>
                    <div class="subMenu_wrapper">
                        <ul>
                            <li>
                                <a href="{{url('search')}}" class="hvr-underline-from-left">Places to Visit</a>
                            </li>
                            <li>
                                <a href="{{url('search')}}" class="hvr-underline-from-left">Restaurants</a>
                            </li>
                            <li>
                                <a href="{{url('search')}}" class="hvr-underline-from-left">Articles</a>
                            </li>
                        </ul>
=======
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        All tours
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php
                        $category=\App\Models\Category::where('parent_id',3)->get();


                        ?>
                        @if(!empty($category))
                            @foreach($category as $cat)
                                    <a class="dropdown-item" href="javascript:void(0);">{{$cat->cat_name}}</a>
                                    <div class="dropdown-divider"></div>
                                @endforeach
                            @endif

                       {{-- <a class="dropdown-item" href="#">Blue lagoon</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Golden Circle</a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Super Jeep</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Snow Mobile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Private  Tours</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Offers</a>--}}

>>>>>>> ae9b0cab3bd5b3770f04b3f7c2bb68bf449834d2
                    </div>
                </li>
                <li class="nav-item submenu column1">
                    <a class="nav-link dropdown-toggle" href="{{url('get-activities/all-tours')}}">  All Tours</a>

                </li>
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" href="{{url('get-activities/all-tours')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--All Tours--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                        {{--<a class="dropdown-item" href="#">Pleaces to Visit</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Restaurants</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Articles</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>

        </div>


        <!----old header--->
        {{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
            {{--<ul class="navbar-nav mr-auto">--}}
                {{--<li class="nav-item active">--}}
                    {{--<a class="nav-link" href="#">Book your Adventures--}}
                        {{--<span class="sr-only">(current)</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#">Day Tours</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#">Northern Lights</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#">Discover Iceland</a>--}}
                {{--</li>--}}

                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"--}}
                       {{--aria-haspopup="true" aria-expanded="false">--}}
                        {{--All tours--}}
                    {{--</a>--}}

                    {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                        {{--<a class="dropdown-item" href="#">Self-Drive Packages</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Blue lagoon</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Golden Circle</a>--}}

                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Super Jeep</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Snow Mobile</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Private  Tours</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Offers</a>--}}

                    {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}

        {{--</div>--}}
        <ul class="account_cart_links my-2 my-lg-0">
            @if(empty(Auth::id()))

                <li>
                    <a href="#" data-toggle="modal" data-target=".user_login">Login</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target=".user_signup">
                        <span>|</span>Signup
                    </a>
                </li>
            @endif
            <li>
                <a href="#">
                    <i class="fas fa-shopping-cart"></i>
                </a>
            </li>
            <li>
                <a href="{{url('search')}}">
                    <i class="fas fa-search"></i>
                </a>
            </li>
            <li class="nav-item dropdown choose_language">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{url('public/images/flags_03.png')}}">
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_03.png')}}">
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_05.png')}}">
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <img src="{{url('public/images/flags_08.png')}}">
                    </a>
                </div>
            </li>
            @if(!empty(Auth::id()))
                <li>
                    <a href="#" class="user">
                        <img src="{{url('public/images/0.jpg')}}">
                    </a>
                    <div class="edit_user">
                        <ul>
                            <li><a href="{{url('edit-profile')}}">Edit Profile</a></li>
                            <li><a href="{{url('change-password')}}">Change Password</a></li>
                            <li><a href="{{url('logout')}}">Logout</a></li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </nav>
</header>
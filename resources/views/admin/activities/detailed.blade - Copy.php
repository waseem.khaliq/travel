@extends('layouts.app')

@section('content')
		<div class="main_wrapper">
		<div class="intro_header">

			<div class="main_content">
				<div class="container">
					<div class="row">
						<div class="grid_section bloggers search_results text-center">
							<h2><span>Popular Places</span></h2>
							<p>Contact a local for insider travel information and personal recomendations
							</p>
							<div class="grid_content row">
								<div class="col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img1.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
<!--															<span>View details for total price</span>-->
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>
								<div class="col-sm-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img2.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
															<span>View details for total price</span>
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>

								<div class="col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img1.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
															<span>View details for total price</span>
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>
								<div class="col-sm-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img2.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
															<span>View details for total price</span>
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>

								<div class="col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img1.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
															<span>View details for total price</span>
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>
								<div class="col-sm-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
									<div class="card text-left">
										<img class="card-img-top" src="images/img2.jpg" alt="Card image cap">
										<div class="card-body">

											<div class="media-body">
												<div class="rateit float-right"></div>
												<h4>Best Attractions by the Ring Road of Iceland</h4>
												<p>
													<img src="images/map-pink.png">
													<span>0.5 mi to Sydney center</span>
												</p>
												<ul>
													<li>
														<strong>1</strong> BR Apartment</li>
													<li>
														<strong>1</strong> BA</li>
													<li>
														<strong>538</strong> sq. ft.</li>
													<li>Sleeps
														<strong>6</strong>
													</li>
												</ul>
												<div class="search_reviews">
													<div class="float-right">
														<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>
													</div>
													<small class="text-success">Very Good! 4.2/5 </small>
													<h5>$137 per night</h5>
													<div class="clearfix">
														<a href="#" data-toggle="modal" data-target=".place_description">
															<span>View details for total price</span>
														</a>

													</div>
													<p class="pt-3 card-text">Iceland has one main ring road, Route 1. How long does it take to drive Iceland's ring road? What are the best attractions you can find along and by <a class="card-title" href="single.html">read more...</a></p>
												</div>
											</div>


										</div>
									</div>
								</div>

							</div>
						</div>

					</div>
				</div>



			</div>
		</div>
	</div>

@endsection
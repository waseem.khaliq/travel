@extends('layouts.master')

@section('title', 'TripXonic | Activities')

@section('styles')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
{!! Html::style('assets/admin/plugins/datatables/datatables.min.css') !!}
{!! Html::style('assets/admin/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}
        <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('content')

        <!-- BEGIN PAGE TITLE-->
<h3 class="page-title"> All Excursions</h3>
<!-- END PAGE TITLE-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('admin/activities') }}">Activities > listing</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            {{--<a href="{{ url('admin/dashboard') }}">--}}
                {{--<button class="btn grey-cascade" type="button"><i class="fa fa-arrow-left"></i> Back</button>--}}
            {{--</a>--}}
        </div>
    </div>
</div>
<!-- End PAGE BAR -->
@if (Session::has('message'))
    <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        {{ Session::get('message') }}
    </div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <span> {{  $errors->first() }} </span>

    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="fa fa-globe"></i>
                    <span class="caption-subject bold uppercase"> Activities</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="{{ url('admin/activities/add') }}" class="btn sbold green"> Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                {{--<div class="col-md-2">--}}
                    {{--<select class="form-control" name="action_type">--}}
                        {{--<option value="">Action</option>--}}
                        {{--<option value="Active">Active</option>--}}
                        {{--<option value="Inactive">Inactive</option>--}}
                        {{--<option value="Delete">Delete</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<button data-toggle="modal" id="test" href="#action_modal" type="submit" class="btn redss">--}}
                    {{--Submit--}}
                {{--</button>--}}
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">

                    <thead>
                    <tr>
                        {{--<th>--}}
                            {{--<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>--}}
                        <th>Sr. #</th>
                        <th> Name</th>
                        <th> SSN</th>
                        <th> Image</th>
                        <th> Status</th>
                        <th> Created</th>
                        <th style="width: 17%"> Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $numbers = 1; ?>
                    @if(sizeof($activity))

                    @foreach($activity as $row)

                        <tr class="odd gradeX">
                            {{--<td>--}}
                                {{--<input name="bulk_id[]" type="checkbox" class="checkboxes" value="{{ $row->id }}"/>--}}
                            {{--</td>--}}
                            <td> {{ $numbers }}</td>
                            <td> {{ $row->place_name }}</td>
                            <td> {{ $row->ssn }}</td>

                            <td>@if(sizeof($row->photo))
                                    <img width="50px" height="50px" src="{{ url('/public/uploads/'.$row->photo[0]->photo)}}">
                                @else
                                    <img width="50px" height="50px" src="{{ url('/public/uploads/no_image.png')}}">
                                @endif
                            </td>
                            <td id="status_{{$row->id}}">{{ $row->status}} </td>
                            <td class="center"> {!! date('d M Y', strtotime($row->created_at)) !!}
                            </td>
                            <td>
                                <form method="get" action="{{url('/admin/activities/'.$row->id.'/edit') }}">
                                    {{--                                    {{ csrf_field() }}--}}
                                    <button title="Edit" type="submit" class="btn blue">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </form>

                                <div id="status_div_{{$row->id}}" class="status_div">
                                    @if($row->status=="Active")
                                        <a href="javascript:void(0);" id="{{$row->id}}" data-ng-switch="Inactive"
                                           title="Active" class="btn green active">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a href="javascript:void(0);" id="{{$row->id}}" data-ng-switch="Active"
                                           title="Inactive" class="btn btn-warning active">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    @endif
                                </div>
                                <a title="Delete" data-toggle="modal" id="{{url('admin/activities/'.$row->id)}}"
                                        href="#draggable" class="btn red">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php $numbers++; ?>
                    @endforeach
                        @else
                        @endif
                    </tbody>
                </table>
                <?php
                echo "Showing ".$activity->firstItem()." to ".$activity->lastItem()." of ".$activity->total()." records";
                echo $activity->render(); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

</div>
<div class="modal fade draggable-modal" id="action_modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Are you sure you want to perform this action?</h4>
            </div>
            {{--<div class="modal-body"> Modal body goes here </div>--}}
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                <form method="post" action="{{url('admin/places/bulk-operations')}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="all_id" id="id_string">
                    <input type="hidden" name="bulk_action" id="bulk_action">
                    <button type="submit" class="btn green">Yes</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade draggable-modal" id="draggable" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Are you sure you want to delete it?</h4>
            </div>
            {{--<div class="modal-body"> Modal body goes here </div>--}}
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                <form method="post" id="del_modal" action="#">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn green">Yes</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" value="/admin/places/status" name="status_path">
@endsection

@section('script')
        <!-- BEGIN PAGE LEVEL Plugins -->
{!! Html::script('assets/admin/js/datatable.js') !!}
{!! Html::script('assets/admin/plugins/datatables/datatables.min.js') !!}
{!! Html::script('assets/admin/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}

        <!-- End PAGE LEVEL Plugins -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! Html::script('assets/admin/js/table-datatables-managed.min.js') !!}
{!! Html::script('assets/admin/js/pages/places.js') !!}
        <!-- END PAGE LEVEL SCRIPTS -->
@endsection

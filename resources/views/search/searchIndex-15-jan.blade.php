@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/js/autocomplete/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('assets/web/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('assets/web/css/daterangepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('assets/web/css/animate.css')}}">
<style>
    /*.search_results .media-body h4 {font-size: 16px;}*/
    .mapsMinMakerDiv{right: -10px;position: relative;top: -6px;}
    #mapCanvasMainDiv.fixed {position: fixed;top: 0; /*padding: 0;*/}
    footer{position: relative;}
    /*search header panel*/
    .search_wrapper .tab-content {margin-top: 10px;}
    .main_slider.bg_slider.bgSliderCustom {height: 0px;min-height: 340px;}
    .search_wrapper.searchWrapper {bottom: 12%;padding: 20px 20px 4px 20px;}
    .form-group {margin-bottom: 0.2rem !important;}
    .search_wrapper .tab-content {margin-top: 12px !important;}
    .view_all {margin: 0px auto 0px !important;}
    #unfavorite {
        display: none;
    }
    .recordNo{
        width: 60%;
        margin: 0 auto;
        border: 1px solid gainsboro;
        padding: 1.5em;
        color: grey;
    }
    .recordNo>h1{
        text-align: center;
    }
    .main_con{
        overflow-x: hidden;
    }

</style>
@section('content')
    {{--<div class="main_wrapper">--}}

    @include('layouts/partial_header')

    <div class="main_content main_con">
        <div class="container">
            <div class="filter">
                <form class="d-flex justify-content-center align-items-center">
                    <!--  <div class="form-group col select_option">
                        <select class="selectpicker">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                        </select>
                    </div>
                    <div class="form-group col select_option">
                        <select class="selectpicker">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                        </select>
                    </div>
                    <div class="form-group col select_option">
                        <select class="selectpicker">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                        </select>
                    </div>
                    <div class="form-group col">
                        <button type="submit" class="btn view_all hvr-float-shadow">Search</button>
                    </div>-->
                </form>
            </div>
        </div>

        <div class="container-fluid">
            <div class="search_results">
                <div class="clearfix">
                    <div class="col maps float-right" style="margin-top: 40px;">
                        <div class="col-md-9 map-right resturant_map hidden-sm" id="mapCanvasMainDiv">
                            <div style="height: 653px;width: 400px;" id="mapCanvas"></div>
                        </div>
                    </div>
                    @if(!empty($places))
                        <?php
                        $i=0;
                        ?>
                        <div class="col custom_results float-left">
                            <div class="d-flex search_sorting">

                                <div class="col">
                                    <?php
                                    /*                                    echo '<pre>';
                                                                        print_r($places->total());
                                                                        exit;

                                                                        */?>
                                    1-{{count($places)}} of {{$places->total()}}
                                </div>
                                <div class="sort_dropdown mr-auto">
                                    <ul>
                                        <li class="nav-item dropdown">
                                            <select class="form-control sort_by select2" style="font-size: 13px;position: relative;top: -7px;" id="sort">
                                                <option value="0">Sort By</option>
                                                <option
                                                        title="Sort By Recent" @if (isset($_GET['sort']) && $_GET['sort'] == "recent") selected="selected" @endif
                                                value="recent">Sort By Recent
                                                </option>
                                                <option
                                                        title="Sort By Name" @if (isset($_GET['sort']) && $_GET['sort'] == "name") selected="selected" @endif
                                                value="name">Sort By Name
                                                </option>
                                                <option
                                                        title="Sort By Rating" @if (isset($_GET['sort']) && $_GET['sort'] == "rating") selected="selected" @endif
                                                value="rating">Sort By Rating
                                                </option>

                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            @foreach($places as $obj)
                                <div class="card text-left">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="custom_checkbox">


                                                <!-- favorite start -->
                                                <div id="unfav-{{@$obj->id}}" data-action="{{@$obj->id}}" class="unfavorite"
                                                     content="{{@$obj->category_id}}" style="display: none">
                                                    <a class="btn_full_outline unfav"
                                                       href="javascript:void(0)">
                                                        <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                </div>
                                                <div id="fav-{{@$obj->id}}" data-action="{{@$obj->id}}"
                                                     content="{{@$obj->category_id}}" class="favorite" style="display: none">
                                                    <a class="btn_full_outline fav"
                                                       href="javascript:void(0)">
                                                        <i class=" icon-heart"></i>  <label for="heart" style="color:black " >❤</label></a>
                                                </div>

                                                @if(isset($obj->favoruite) && !empty($obj->favoruite))
                                                    <div id="unfavorite-{{@$obj->id}}" class="">
                                                        <a class="btn_full_outline unfavorite" content="{{@$obj->category_id}}"
                                                           data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                            <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                    </div>
                                                @else
                                                    <div id="favorite-{{@$obj->id}}" class="">
                                                        <a class="btn_full_outline favorite" content="{{@$obj->category_id}}"
                                                           data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                            <i class=" icon-heart"></i> <label for="heart" style="color:black ">❤</label></a>

                                                    </div>
                                                @endif


                                                {{--<input id="heart" class="heart" type="checkbox" />
                                                                                                    <label for="heart">❤</label>--}}
                                            </div>
                                            {{-- <a href="#">
                                                 <img src="images/landscape24.jpg" alt="Generic placeholder image">

                                             </a>--}}
                                            @if(Request::segment(1)=='restaurants' || Request::segment(1)=='get-restaurants-by')
                                                <a href="{{url('/restaurants/detail/'.$obj->slug)}}" class="hvr-float-shadow ">
                                                    @elseif(Request::segment(1)=='places')
                                                        <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow "></a>
                                                    @else
                                                        <a href="{{url('/activities/detail/'.$obj->slug)}}" class="hvr-float-shadow "></a>
                                                    @endif
                                                    {{--@if(!empty($obj->single_photo->photo) && file_exists(public_path('public/uploads/'.@$obj->single_photo->photo)))--}}
                                                    @if(!empty($obj->single_photo->photo))
                                                        @if($obj->category_id==3)
                                                            <img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                        @else
                                                            <img src="{{url('public/uploads/'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                        @endif
                                                        {{--<img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">--}}
                                                    @else
                                                        <img src="{{url('/public/images/no-image.png')}}" class="img-responsive" alt="image">
                                                    @endif
                                                </a>
                                                <div class="media-body">

                                                    <h4>{{@$obj->place_name}}</h4>
                                                    <h4>{{@$obj->restaurant_name}}</h4>
                                                    <h4>{{@$obj->activity_name}}</h4>
                                                    {{--<p>--}}
                                                    {{--<img src="images/map-pink.png">--}}

                                                    {{--<div class="page-container">
                                                        <div class="container">
                                                            <br />
                                                            <button class="btn launch-map">Launch Map</button>
                                                        </div>
                                                    </div>--}}
                                                    <div id="modal" class="modal hide fade">
                                                        <div class="modal-body">
                                                            <div id="map-canvas"></div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-primary" data-value="1">Continue</button>
                                                            <button type="button" data-dismiss="modal" class="btn" data-value="0">Cancel</button>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-1 mapsMinMakerDiv" style="line-height: 0px;">
                                                            <span lat="{{@$obj->address->latitude}}"
                                                                  long="{{@$obj->address->longitude}}"
                                                                  hotelName="{{@$obj->place_name}}{{@$obj->restaurant_name}}"
                                                                  href="javascript:void(0)"
                                                                  class="spc--right--med jq-map-link map_modal_show  launch-map"
                                                                  data-toggle="modal"
                                                                  data-target="#myMap">
                                                            <i class="fa icon-down-open"></i> <img src="{{url('public/images/map-pink.png')}}">
                                                        </span>
                                                        </div>
                                                        <div class="col-md-10" style="padding-left: 0px">
                                                            <span>{{@$obj->address->address.', '.@$obj->address->city}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="discription">
                                                        <p style="color: #7d6e6e;">
                                                            {{str_limit(strip_tags(html_entity_decode(trim($obj->description) )),200, '...')}}
                                                        </p>
                                                    </div>
                                                    <div class="search_reviews">
                                                        <div class="float-right">
                                                            @if(!empty($obj->phone))
                                                                <i class="fa fa-phone"></i>:{{$obj->phone}}
                                                            @endif
                                                            <br>
                                                            {{--<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>--}}
                                                            @if(Request::segment(1)=='restaurants' || Request::segment(1)=='get-restaurants-by')
                                                                <a href="{{url('/restaurants/detail/'.$obj->slug)}}" class="hvr-float-shadow view_all">Detail</a>
                                                            @elseif(Request::segment(1)=='places')
                                                                <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow view_all">Detail</a>
                                                            @else
                                                                <a href="{{url('/activities/detail/'.$obj->slug)}}" class="hvr-float-shadow view_all">Book Now</a>
                                                            @endif
                                                        </div>
                                                        {{--<small class="text-success">Very Good! 4.2/5 </small>--}}

                                                        @if(Request::segment(1)=='restaurants' || Request::segment(1)=='get-restaurants-by')
                                                            {{--<h5>$137 per night</h5>--}}
                                                        @elseif(Request::segment(1)=='places')
                                                            {{--<h5>$137 per night</h5>--}}
                                                        @else
                                                            @if(!empty($obj->price))
                                                                <h5>£{{$obj->price}}</h5>
                                                            @endif
                                                        @endif

                                                        <div class="clearfix">
                                                            <a href="#" data-toggle="modal" data-target=".place_description"></a>
                                                            <div class=" float-left">
                                                                @if(isset($obj->reviews_avg->rating) && !empty($obj->reviews_avg->rating))
                                                                    <div class="rating">

                                                                        <div class="rateYo-{{$i}}"></div>
                                                                        <small>
                                                                        </small>
                                                                        <?php
                                                                        if($obj->reviews_avg->rating>5){
                                                                            // echo 'greater';
                                                                            $star_rating='5';
                                                                        }else{
                                                                            // echo 'less';
                                                                            $star_rating=@$obj->reviews_avg->rating;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            showRating(<?=$i?>, <?=@$star_rating?>);
                                                                        });
                                                                    </script>
                                                                @endif
                                                            </div>
                                                            <div class="row margin_top2em">
                                                                @if(!empty($obj->website_url))
                                                                    <div class="col-md-2">
                                                                        <a href="{{@$obj->website_url}}"><i class="fa fa-chrome"></i></a>
                                                                    </div>
                                                                @endif
                                                                @if(!empty($obj->social_1))
                                                                    <div class="col-md-2">
                                                                        <a href="{{@$obj->social_1}}"><i class="fa fa-facebook-f"></i></a>
                                                                    </div>
                                                                @endif
                                                                @if(!empty($obj->social_2))
                                                                    <div class="col-md-2">
                                                                        <a href="{{@$obj->social_2}}"><i class="fa fa-twitter"></i></a>
                                                                    </div>
                                                                @endif
                                                                @if(!empty($obj->social_3))
                                                                    <div class="col-md-2">
                                                                        <a href="{{@$obj->social_3}}"><i class="fa fa-instagram"></i></a>
                                                                    </div>
                                                                @endif
                                                                @if(!empty($obj->social_4))
                                                                    <div class="col-md-2">
                                                                        <a href="{{@$obj->social_4}}"><i class="fa fa-tripadvisor"></i></a>
                                                                    </div>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>


                                    </div>

                                </div>




                            @endforeach
                            <div class="col-12 mt-4 float-left paginateDivMain">
                                <nav aria-label="page navigation example">

                                    {{--{{ $places->links() }}--}}
                                    {{$places->appends(['sort' => 'votes'])->render()}}
                                </nav>
                            </div>


                        </div>

                    @endif
                    @if(empty($places))

                        <div class="recordNo">

                            <h5>No record found</h5>
                        </div>


                    @endif

                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}
    <input type="hidden" value="{{Auth::id()}}" id="auth_user_login" name="">
    {!! Html::script('assets/web/js/bootstrap.min.js') !!}
    {!! Html::script('assets/web/js/moment.js') !!}
    {!! Html::script('assets/web/js/favorite.js') !!}
    <?php include(base_path('assets/web/pages/searchMpas.php'));?>
    {!! Html::script('assets/web/js/jquery.rateyo.min.js') !!}
    <!-- auto complete start -->
    {!! Html::script('assets/admin/js/autocomplete/jquery-ui.js') !!}
    {!! Html::script('assets/web/js/searchAutoComplete.js') !!}



    {!! Html::script('assets/web/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('assets/web/js/daterangepicker.js') !!}
    {!! Html::script('assets/web/js/jquery.datetimepicker.full.min.js') !!}


    <script>
        $(document).ready(function () {
            $('.daterange').daterangepicker({
                timePicker: true,
                /*startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),*/
                locale: {
                    format: 'DD/MM/YY hh:mm A'
                }
            });

// bootstrap select
        });
    </script>
    <script type="text/javascript">
        var num = 750; //number of pixels before modifying styles

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                $('#mapCanvasMainDiv').addClass('fixed');
            } else {
                $('#mapCanvasMainDiv').removeClass('fixed');
            }
        });
        $('#autocomplete').autocomplete({
            source: 'getSearch',
            appendTo: "#options"
        });
    </script>
    <script type="text/javascript">


        $("#act_name").autocomplete({
            source: web_url + "/search/SearchActAutoName",
            appendTo:'#activityoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#act_city_id').val(e.id);
                $('input[name=activity_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#act_name').val() === 'No records found') {
                    $("#act_name").val("");
                    $("#act_name").attr("disabled", false);
                }
            }

        });


        $("#plc_name").autocomplete({
            source: web_url + "/search/SearchPlcAutoName",
            appendTo:'#placeoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#place_city_id').val(e.id);
                $('input[name=search_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#plc_name').val() === 'No records found') {
                    $("#plc_name").val("");
                    $("#plc_name").attr("disabled", false);
                }
            }



        });
        $("#rest_name_search").autocomplete({
            source: web_url + "/search/searchAutoRestName",
            appendTo:'#restaurantoSearchOptions',
            select: function (event, ui) {
                var e = ui.item;
                $('input[name=city_id]').val(e.id);
                $('input[name=type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#rest_name_search').val() === 'No records found') {
                    $("#rest_name_search").val("");
                    $("#rest_name_search").attr("disabled", false);
                }
            }
        });

    </script>
    <script>
        // function initialize()
        // {
        //     var myLatLng = new google.maps.LatLng(28.617161,77.208111);
        //     var map = new google.maps.Map(document.getElementById("map"),
        //         {
        //             zoom: 17,
        //             center: myLatLng,
        //             mapTypeId: google.maps.MapTypeId.ROADMAP
        //         });
        //     var marker = new google.maps.Marker(
        //         {
        //             position: myLatLng,
        //             map: map,
        //             title: 'Rajya Sabha'
        //         });
        // }
        $(document).ready(function () {
            // initialize();
            $(document).on("click", ".map_modal_show", function () {

                var hotelName = ($(this).attr('hotelName'));
                var lat = ($(this).attr('lat'));
                var long = ($(this).attr('long'));
                //alert(lat);
                // alert(long);
                // alert(hotelName);


            });
        });

    </script>
    <!-- auto complete end -->


@endsection
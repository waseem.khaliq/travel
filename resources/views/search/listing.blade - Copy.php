@extends('layouts.app')

<link rel="stylesheet" type="text/css" href="{{url('assets/admin/js/autocomplete/jquery-ui.css')}}">
{!! Html::style('assets/web/css/jquery.rateyo.min.css') !!}
<style>
    .main_slider.bg_slider.bgSliderCustom {
        height: 0px;
        min-height: 306px;
    }
    #unfavorite {
        display: none;
    }
    .search_wrapper.searchWrapper {
        bottom: 20%;
        padding: 20px 20px 4px 20px;
    }
    .recordNo{
        width: 60%;
        margin: 0 auto;
        border: 1px solid gainsboro;
        padding: 1.5em;
        color: grey;
    }
    .recordNo>h1{
        text-align: center;
    }
</style>

@section('content')

<div class="main_wrapper">


    <div class="main_slider bg_slider bgSliderCustom">
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/1.png')}}"
                         alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/2.png')}}"
                         alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/3.png')}}"
                         alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/4.png')}}"
                         alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/5.png')}}"
                         alt="Third slide">
                </div>
                {{--<div class="carousel-item">--}}
                    {{--<img class="d-block w-100 h-100" src="{{url('public/images/lancscape6.jpg')}}"--}}
                         {{--alt="Second slide">--}}
                {{--</div>--}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="search_wrapper searchWrapper">
            {{-- <ul class="nav nav-tabs" id="myTab" role="tablist">

                 <li class="nav-item">
                     <a class="nav-link @if(isset($_GET['activtiy_term'])) active @endif" id="Vacation-tab" data-toggle="tab" href="#Vacation" role="tab" aria-controls="Vacation" aria-selected="false">
                         <i class="fas fa-tree"></i>Activities</a>
                 </li>

             </ul>--}}
            <div>

                <div class=" ">
                    <form action="{{url('places/search')}}" method="get" id="form_search_act">
                        <div class="row">
                            <div class="form-group col-md-5" style="margin-bottom: 0px">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <!-- <label>Search Restaurants</label> -->
                                    <input name="city_id" id="place_city_id" value="0" type="hidden">
                                    <input name="search_type" id="search_type" value="0" type="hidden">
                                    <input type="text" name="city"
                                           value="@if(isset($_GET['city']) && $_GET['city']!=""){{trim($_GET['city'])}} @endif"
                                             class="form-control  input_hotel" id="plc_name"
                                           placeholder="City, Place name">
                                    <span class="icon icon-location" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12" id="activityoptions">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group  col-md-5" style="margin-bottom: 0px;margin-top: 8px;">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <!-- <label>Food type</label> -->
                                    <select class="ddslick form-control" name="cuisine" id="style-6">
                                        <option value=""
                                                selected>Places
                                        </option>
                                        @if(!empty($subcat_place))
                                            @foreach($subcat_place as $sub_cat)
                                                <option value="{{$sub_cat->id}}" @if(isset($sub_cat_detail->id)
                                             && $sub_cat_detail->id==$sub_cat->id ) selected="selected" @endif >{{@$sub_cat->cat_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-2">
                                <button type="submit" class="btn hvr-float-shadow view_all">Search</button>
                            </div>
                        </div>
                        {{-- <div class="row">
                             <div class="form-group col-12" id="options">

                             </div>
                         </div>--}}


                    </form>
                </div>

            </div>
        </div>
    </div>

        <div class="main_content">
            <div class="container">

                <div class="filter">
                  <!--  <form class="d-flex justify-content-center align-items-center">
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <button type="submit" class="btn view_all hvr-float-shadow">Search</button>
                        </div>
                    </form>-->

                </div>
            </div>
            <div class="container-fluid">
                <div class="search_results">
                    {{--<div style="height:150px;"></div>--}}
                    {{--<h2 style="margin-top:30px; margin-bottom:40px;"> {{$sub_cat_detail->cat_name}}</h2>--}}

                    <div class="clearfix" >
                        <div class="col maps float-right">

                        <div class="col-md-9 map-right resturant_map hidden-sm">
                                <div style="height: 600px;width: 400px;" id="mapCanvas"></div>
                         </div>
                        </div>
                        <div class="col custom_results float-left">

                        @if(!empty($places))
                            @foreach($places as $obj)


                                    <div class="card text-left">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="custom_checkbox">
                                                    <!-- favorite start -->
                                                    <div id="unfav-{{@$obj->id}}" data-action="{{@$obj->id}}" class="unfavorite"
                                                         content="{{@$obj->category_id}}" style="display: none">
                                                        <a class="btn_full_outline unfav"
                                                           href="javascript:void(0)">
                                                            <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                    </div>
                                                    <div id="fav-{{@$obj->id}}" data-action="{{@$obj->id}}"
                                                         content="{{@$obj->category_id}}" class="favorite" style="display: none">
                                                        <a class="btn_full_outline fav"
                                                           href="javascript:void(0)">
                                                            <i class=" icon-heart"></i>  <label for="heart" style="color:black " >❤</label></a>
                                                    </div>
                                                    <?php
                                                    /*echo '<pre>';
                                                    print_r($favoruite);
                                                    exit;*/
                                                    ?>
                                                    @if(isset($obj->favoruite) && !empty($obj->favoruite))
                                                        <div id="unfavorite-{{@$obj->id}}" class="">
                                                            <a class="btn_full_outline unfavorite" content="{{@$obj->category_id}}"
                                                               data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                                <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                        </div>
                                                    @else
                                                        <div id="favorite-{{@$obj->id}}" class="">
                                                            <a class="btn_full_outline favorite" content="{{@$obj->category_id}}"
                                                               data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                                <i class=" icon-heart"></i> <label for="heart" style="color:black ">❤</label></a>

                                                        </div>
                                                    @endif
                                                    {{--<input id="heart" class="heart" type="checkbox" />
                                                    <label for="heart">❤</label>--}}
                                                </div>
                                                {{-- <a href="#">
                                                     <img src="images/landscape24.jpg" alt="Generic placeholder image">

                                                 </a>--}}

                                                            <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow ">

                                                                        {{--@if(!empty($obj->single_photo->photo) && file_exists(public_path('public/uploads/'.@$obj->single_photo->photo)))--}}
                                                                        @if(!empty($obj->single_photo->photo))
                                                                            @if($obj->category_id==3)
                                                                                <img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                                            @else
                                                                                <img src="{{url('public/uploads/'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                                            @endif
                                                                            {{--<img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">--}}
                                                                        @else
                                                                            <img src="{{url('/public/images/no-image.png')}}" class="img-responsive" alt="image">
                                                                        @endif

                                                                    </a>

                                                                    <div class="media-body">

                                                                        <div class=" float-right">
                                                                            @if(isset($obj->reviews_avg->rating) && !empty($obj->reviews_avg->rating))
                                                                                <div class="rating">

                                                                                    <div class="rateYo-{{$i}}"></div>
                                                                                    <small>
                                                                                    </small>
                                                                                    <?php
                                                                                    if($obj->reviews_avg->rating>5){
                                                                                        // echo 'greater';
                                                                                        $star_rating='5';
                                                                                    }else{
                                                                                        // echo 'less';
                                                                                        $star_rating=@$obj->reviews_avg->rating;
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <script>
                                                                                    $(document).ready(function () {
                                                                                        showRating(<?=$i?>, <?=@$star_rating?>);
                                                                                    });
                                                                                </script>
                                                                            @endif
                                                                        </div>
                                                                        {{--<h4>College st 1 Bedroom with Balcony</h4>--}}


                                                                        <h4>{{@$obj->place_name}}</h4>

                                                                        <p>
                                                                        {{--<img src="images/map-pink.png">--}}
                                                                        <?php
                                                                        /*  echo '<pre>';
                                                                          print_r($obj);
                                                                          exit;*/

                                                                        ?>

                                                                        {{--<div class="page-container">
                                                                            <div class="container">
                                                                                <br />
                                                                                <button class="btn launch-map">Launch Map</button>
                                                                            </div>
                                                                        </div>--}}
                                                                        <div id="modal" class="modal hide fade">
                                                                            <div class="modal-body">
                                                                                <div id="map-canvas"></div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" data-dismiss="modal" class="btn btn-primary" data-value="1">Continue</button>
                                                                                <button type="button" data-dismiss="modal" class="btn" data-value="0">Cancel</button>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-1 mapsMinMakerDiv" style="line-height: 0px;">
                                                            <span lat="{{@$obj->address->latitude}}"
                                                                  long="{{@$obj->address->longitude}}"
                                                                  hotelName="{{@$obj->place_name}}"
                                                                  href="javascript:void(0)"
                                                                  class="spc--right--med jq-map-link map_modal_show  launch-map"
                                                                  data-toggle="modal"
                                                                  data-target="#myMap">
                                                            <i class="fa icon-down-open"></i> <img src="{{url('public/images/map-pink.png')}}">
                                                        </span>
                                                                            </div>
                                                                            <div class="col-md-10" style="padding-left: 0px">
                                                                                <span>{{@$obj->address->address.', '.$obj->address->city}}</span>
                                                                            </div>
                                                                        </div>


                                                                     {{--   <ul>
                                                                            <li>
                                                                                <strong>1</strong> BR Apartment</li>
                                                                            <li>
                                                                                <strong>1</strong> BA</li>
                                                                            <li>
                                                                                <strong>538</strong> sq. ft.</li>
                                                                            <li>Sleeps
                                                                                <strong>6</strong>
                                                                            </li>
                                                                        </ul>--}}
                                                                        <div class="discription">
                                                                            <p>
                                                                                {{str_limit(strip_tags(html_entity_decode(trim($obj->description))),200, '...')}}
                                                                            </p>
                                                                        </div>
                                                                        <div class="search_reviews">
                                                                            <div class="float-right">
                                                                                {{--<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>--}}

                                                                                    <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow view_all">Detail</a>

                                                                            </div>
                                                                            {{--<small class="text-success">Very Good! 4.2/5 </small>--}}
                                                                            {{--<h5>$137 per night</h5>--}}
                                                                            <div class="clearfix">

                                                                               <!-- <a href="#" data-toggle="modal" data-target=".place_description">
                                                                                    <span>View details for total price</span>
                                                                             </a>-->
                                                                                <div class="rateit float-left"></div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                            </div>

                                        </div>

                                    </div>
                                @endforeach
                    <div class="col-12 mt-4 float-left paginateDivMain">
                            <nav aria-label="page navigation example">

                              {{ $places->links() }}
                            </nav>
                        </div>


                            @endif

                        </div>

                        @if(empty($places))

                                    <div class="recordNo">

                                        <h5>No record found</h5>
                                    </div>


                        @endif
                    </div>

                </div>

            </div>
        </div>

    </div>

<input type="hidden" value="{{Auth::id()}}" id="auth_user_login" name="">
{!! Html::script('assets/web/js/favorite.js') !!}
<?php include(base_path('assets/web/pages/searchMpas.php'));?>

<!-- auto complete start -->
     {!! Html::script('assets/admin/js/autocomplete/jquery-ui.js') !!}

{!! Html::script('assets/web/js/jquery.rateyo.min.js') !!}
    {!! Html::script('assets/web/js/searchAutoComplete.js') !!}

<script type="text/javascript">

$('#autocomplete').autocomplete({
source: 'getSearch',
appendTo: "#options"
});
</script>
<script type="text/javascript">


      $("#act_name").autocomplete({
            source: web_url + "/search/SearchActAutoName",
            appendTo:'#activityoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#act_city_id').val(e.id);
                $('input[name=activity_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#act_name').val() === 'No records found') {
                    $("#act_name").val("");
                    $("#act_name").attr("disabled", false);
                }
            }

        });


      $("#plc_name").autocomplete({
            source: web_url + "/search/SearchPlcAutoName",
            appendTo:'#placeoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#place_city_id').val(e.id);
                $('input[name=search_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#plc_name').val() === 'No records found') {
                    $("#plc_name").val("");
                    $("#plc_name").attr("disabled", false);
                }
            }



        });
        $("#rest_name_search").autocomplete({
            source: web_url + "/search/searchAutoRestName",
            appendTo:'#restaurantoSearchOptions',
            select: function (event, ui) {
                var e = ui.item;
                $('input[name=city_id]').val(e.id);
                $('input[name=type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#rest_name_search').val() === 'No records found') {
                    $("#rest_name_search").val("");
                    $("#rest_name_search").attr("disabled", false);
                }
            }
        });

</script>
<!-- auto complete end -->


@endsection
@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/js/autocomplete/jquery-ui.css')}}">
{!! Html::style('assets/web/css/jquery.rateyo.min.css') !!}
<style>
    .main_slider.bg_slider.bgSliderCustom {
        height: 0px;
        min-height: 306px;
    }
    #unfavorite {
        display: none;
    }
    .search_wrapper.searchWrapper {
        bottom: 20%;
        padding: 20px 20px 4px 20px;
    }
    .recordNo{
        width: 60%;
        margin: 0 auto;
        border: 1px solid gainsboro;
        padding: 1.5em;
        color: grey;
    }
    .recordNo>h1{
        text-align: center;
    }
</style>
@section('content')
<div class="main_wrapper">


    <div class="main_slider bg_slider bgSliderCustom">
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/1.png')}}"
                         alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/2.png')}}"
                         alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/3.png')}}"
                         alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/4.png')}}"
                         alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-100" src="{{url('public/images/search/5.png')}}"
                         alt="Third slide">
                </div>
                {{--<div class="carousel-item">--}}
                    {{--<img class="d-block w-100 h-100" src="{{url('public/images/lancscape6.jpg')}}"--}}
                         {{--alt="Second slide">--}}
                {{--</div>--}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="search_wrapper searchWrapper">
            {{-- <ul class="nav nav-tabs" id="myTab" role="tablist">

                 <li class="nav-item">
                     <a class="nav-link @if(isset($_GET['activtiy_term'])) active @endif" id="Vacation-tab" data-toggle="tab" href="#Vacation" role="tab" aria-controls="Vacation" aria-selected="false">
                         <i class="fas fa-tree"></i>Activities</a>
                 </li>

             </ul>--}}
            <div>

                <div class=" ">
                    <form action="{{url('places/search')}}" method="get" id="form_search_act">
                        <div class="row">
                            <div class="form-group col-md-5" style="margin-bottom: 0px">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <!-- <label>Search Restaurants</label> -->
                                    <input name="city_id" id="place_city_id" value="0" type="hidden">
                                    <input name="search_type" id="search_type" value="0" type="hidden">
                                    <input type="text" name="city"
                                           value="@if(isset($_GET['city']) && $_GET['city']!=""){{trim($_GET['city'])}} @endif"
                                           required  class="form-control  input_hotel" id="plc_name"
                                           placeholder="City, Place name">
                                    <span class="icon icon-location" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12" id="activityoptions">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group  col-md-5" style="margin-bottom: 0px;margin-top: 8px;">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <!-- <label>Food type</label> -->
                                    <select class="ddslick form-control" name="cuisine" id="style-6">
                                        <option value=""
                                                selected>Places
                                        </option>
                                        @if(!empty($subcat_place))
                                            @foreach($subcat_place as $sub_cat)
                                                <option value="{{$sub_cat->id}}" @if(isset($sub_cat_detail->id)
                                             && $sub_cat_detail->id==$sub_cat->id ) selected="selected" @endif >{{@$sub_cat->cat_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-2">
                                <button type="submit" class="btn hvr-float-shadow view_all">Search</button>
                            </div>
                        </div>
                        {{-- <div class="row">
                             <div class="form-group col-12" id="options">

                             </div>
                         </div>--}}


                    </form>
                </div>

            </div>
        </div>
    </div>

        <div class="main_content">
            <div class="container">

                <div class="filter">
                  <!--  <form class="d-flex justify-content-center align-items-center">
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col select_option">
                            <select class="selectpicker">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <button type="submit" class="btn view_all hvr-float-shadow">Search</button>
                        </div>
                    </form>-->

                </div>
            </div>
            <div class="container-fluid">
                <div class="search_results">
                    {{--<div style="height:150px;"></div>--}}
                    {{--<h2 style="margin-top:30px; margin-bottom:40px;"> {{$sub_cat_detail->cat_name}}</h2>--}}

                    <div class="clearfix" >
                        <div class="col maps float-right">

                        <div class="col-md-9 map-right resturant_map hidden-sm">
                                <div style="height: 600px;width: 400px;" id="mapCanvas"></div>
                         </div>
                        </div>
                        <div class="col custom_results float-left">

                        @if(!empty($places))
                            @foreach($places as $obj)


                                    <div class="card text-left">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="custom_checkbox">
                                                    <!-- favorite start -->
                                                    <div id="unfav-{{@$obj->id}}" data-action="{{@$obj->id}}" class="unfavorite"
                                                         content="{{@$obj->category_id}}" style="display: none">
                                                        <a class="btn_full_outline unfav"
                                                           href="javascript:void(0)">
                                                            <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                    </div>
                                                    <div id="fav-{{@$obj->id}}" data-action="{{@$obj->id}}"
                                                         content="{{@$obj->category_id}}" class="favorite" style="display: none">
                                                        <a class="btn_full_outline fav"
                                                           href="javascript:void(0)">
                                                            <i class=" icon-heart"></i>  <label for="heart" style="color:black " >❤</label></a>
                                                    </div>
                                                    <?php
                                                    /*echo '<pre>';
                                                    print_r($favoruite);
                                                    exit;*/
                                                    ?>
                                                    @if(isset($obj->favoruite) && !empty($obj->favoruite))
                                                        <div id="unfavorite-{{@$obj->id}}" class="">
                                                            <a class="btn_full_outline unfavorite" content="{{@$obj->category_id}}"
                                                               data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                                <i class=" icon-heart"></i>  <label for="heart" style="color: red">❤</label></a>
                                                        </div>
                                                    @else
                                                        <div id="favorite-{{@$obj->id}}" class="">
                                                            <a class="btn_full_outline favorite" content="{{@$obj->category_id}}"
                                                               data-action="{{@$obj->id}}" href="javascript:void(0)">
                                                                <i class=" icon-heart"></i> <label for="heart" style="color:black ">❤</label></a>

                                                        </div>
                                                    @endif
                                                    {{--<input id="heart" class="heart" type="checkbox" />
                                                    <label for="heart">❤</label>--}}
                                                </div>
                                                {{-- <a href="#">
                                                     <img src="images/landscape24.jpg" alt="Generic placeholder image">

                                                 </a>--}}

                                                            <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow ">

                                                                        {{--@if(!empty($obj->single_photo->photo) && file_exists(public_path('public/uploads/'.@$obj->single_photo->photo)))--}}
                                                                        @if(!empty($obj->single_photo->photo))
                                                                            @if($obj->category_id==3)
                                                                                <img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                                            @else
                                                                                <img src="{{url('public/uploads/'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">
                                                                            @endif
                                                                            {{--<img src="{{url('public/uploads/activities'.@$obj->single_photo->photo)}}" alt="{{@$obj->place_name}}">--}}
                                                                        @else
                                                                            <img src="{{url('/public/images/no-image.png')}}" class="img-responsive" alt="image">
                                                                        @endif

                                                                    </a>

                                                                    <div class="media-body">

                                                                        <div class=" float-right">
                                                                            @if(isset($obj->reviews_avg->rating) && !empty($obj->reviews_avg->rating))
                                                                                <div class="rating">

                                                                                    <div class="rateYo-{{$i}}"></div>
                                                                                    <small>
                                                                                    </small>
                                                                                    <?php
                                                                                    if($obj->reviews_avg->rating>5){
                                                                                        // echo 'greater';
                                                                                        $star_rating='5';
                                                                                    }else{
                                                                                        // echo 'less';
                                                                                        $star_rating=@$obj->reviews_avg->rating;
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <script>
                                                                                    $(document).ready(function () {
                                                                                        showRating(<?=$i?>, <?=@$star_rating?>);
                                                                                    });
                                                                                </script>
                                                                            @endif
                                                                        </div>
                                                                        {{--<h4>College st 1 Bedroom with Balcony</h4>--}}


                                                                        <h4>{{@$obj->place_name}}</h4>

                                                                        <p>
                                                                        {{--<img src="images/map-pink.png">--}}
                                                                        <?php
                                                                        /*  echo '<pre>';
                                                                          print_r($obj);
                                                                          exit;*/

                                                                        ?>

                                                                        {{--<div class="page-container">
                                                                            <div class="container">
                                                                                <br />
                                                                                <button class="btn launch-map">Launch Map</button>
                                                                            </div>
                                                                        </div>--}}
                                                                        <div id="modal" class="modal hide fade">
                                                                            <div class="modal-body">
                                                                                <div id="map-canvas"></div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" data-dismiss="modal" class="btn btn-primary" data-value="1">Continue</button>
                                                                                <button type="button" data-dismiss="modal" class="btn" data-value="0">Cancel</button>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-1 mapsMinMakerDiv" style="line-height: 0px;">
                                                            <span lat="{{@$obj->address->latitude}}"
                                                                  long="{{@$obj->address->longitude}}"
                                                                  hotelName="{{@$obj->place_name}}"
                                                                  href="javascript:void(0)"
                                                                  class="spc--right--med jq-map-link map_modal_show  launch-map"
                                                                  data-toggle="modal"
                                                                  data-target="#myMap">
                                                            <i class="fa icon-down-open"></i> <img src="{{url('public/images/map-pink.png')}}">
                                                        </span>
                                                                            </div>
                                                                            <div class="col-md-10" style="padding-left: 0px">
                                                                                <span>{{@$obj->address->address.', '.$obj->address->city}}</span>
                                                                            </div>
                                                                        </div>


                                                                     {{--   <ul>
                                                                            <li>
                                                                                <strong>1</strong> BR Apartment</li>
                                                                            <li>
                                                                                <strong>1</strong> BA</li>
                                                                            <li>
                                                                                <strong>538</strong> sq. ft.</li>
                                                                            <li>Sleeps
                                                                                <strong>6</strong>
                                                                            </li>
                                                                        </ul>--}}
                                                                        <div class="discription">
                                                                            <p>
                                                                                {{str_limit(strip_tags(html_entity_decode(trim($obj->description))),200, '...')}}
                                                                            </p>
                                                                        </div>
                                                                        <div class="search_reviews">
                                                                            <div class="float-right">
                                                                                {{--<a href="checkout.html" class="hvr-float-shadow view_all">Book Now</a>--}}

                                                                                    <a href="{{url('/place/detail/'.$obj->slug)}}" class="hvr-float-shadow view_all">Detail</a>

                                                                            </div>
                                                                            {{--<small class="text-success">Very Good! 4.2/5 </small>--}}
                                                                            {{--<h5>$137 per night</h5>--}}
                                                                            <div class="clearfix">

                                                                               <!-- <a href="#" data-toggle="modal" data-target=".place_description">
                                                                                    <span>View details for total price</span>
                                                                             </a>-->
                                                                                <div class="rateit float-left"></div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                            </div>

                                        </div>

                                    </div>
                                @endforeach
                    <div class="col-12 mt-4 float-left paginateDivMain">
                            <nav aria-label="page navigation example">

                              {{ $places->links() }}
                            </nav>
                        </div>


                            @endif

                        </div>

                        @if(empty($places))

                                    <div class="recordNo">

                                        <h5>No record found</h5>
                                    </div>


                        @endif
                    </div>

                </div>

            </div>
        </div>

    </div>
<input type="hidden" value="{{Auth::id()}}" id="auth_user_login" name="">
{!! Html::script('assets/web/js/favorite.js') !!}
<?php include(base_path('assets/web/pages/searchMpas.php'));?>

<!-- auto complete start -->
     {!! Html::script('assets/admin/js/autocomplete/jquery-ui.js') !!}

{!! Html::script('assets/web/js/jquery.rateyo.min.js') !!}
    {!! Html::script('assets/web/js/searchAutoComplete.js') !!}

<script type="text/javascript">

$('#autocomplete').autocomplete({
source: 'getSearch',
appendTo: "#options"
});
</script>
<script type="text/javascript">


      $("#act_name").autocomplete({
            source: web_url + "/search/SearchActAutoName",
            appendTo:'#activityoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#act_city_id').val(e.id);
                $('input[name=activity_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#act_name').val() === 'No records found') {
                    $("#act_name").val("");
                    $("#act_name").attr("disabled", false);
                }
            }

        });


      $("#plc_name").autocomplete({
            source: web_url + "/search/SearchPlcAutoName",
            appendTo:'#placeoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#place_city_id').val(e.id);
                $('input[name=search_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#plc_name').val() === 'No records found') {
                    $("#plc_name").val("");
                    $("#plc_name").attr("disabled", false);
                }
            }



        });
        $("#rest_name_search").autocomplete({
            source: web_url + "/search/searchAutoRestName",
            appendTo:'#restaurantoSearchOptions',
            select: function (event, ui) {
                var e = ui.item;
                $('input[name=city_id]').val(e.id);
                $('input[name=type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#rest_name_search').val() === 'No records found') {
                    $("#rest_name_search").val("");
                    $("#rest_name_search").attr("disabled", false);
                }
            }
        });

</script>
<!-- auto complete end -->


@endsection
@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/js/autocomplete/jquery-ui.css')}}">
<!--  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" /> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css" />
{{--autocomplete--}} -->
<!-- 
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js"></script> -->
 
    <style>
/*  .ui-autocomplete {
    position: absolute;
    z-index: 1000;
    cursor: default;
    padding: 0;
    margin-top: 2px;
    list-style: none;
    background-color: #ffffff;
    border: 1px solid #ccc;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
       -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
}
.ui-autocomplete > li {
  padding: 3px 20px;
}
.ui-autocomplete > li.ui-state-focus {
  background-color: #DDD;
}
.ui-helper-hidden-accessible {
  display: none;
}
.ui-menu-item-wrapper:focus{
        background: red;
}
*/
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.recordNo>h1{text-align: center;}
</style>

    <div class="main_wrapper">
        <div class="main_slider bg_slider">
            <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 h-100" src="{{url('public/images/search/1.png')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 h-100" src="{{url('public/images/search/2.png')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 h-100" src="{{url('public/images/search/3.png')}}" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 h-100" src="{{url('public/images/search/4.png')}}" alt="Fourth slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 h-100" src="{{url('public/images/search/5.png')}}" alt="Fifth slide">
                    </div>

                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="search_wrapper">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                   
                    <li class="nav-item">
                        <a class="nav-link active" id="Vacation-tab" data-toggle="tab" href="#Vacation" role="tab" aria-controls="Vacation" aria-selected="false">
                            <i class="fas fa-tree"></i>Activities</a>
                    </li>

                    <li class="nav-item" id="restaurantTab">
                        <a class="nav-link" id="Discover-tab" data-toggle="tab"  href="#Discover" role="tab"
                           aria-controls="Discover" aria-selected="false">
                            <i class="fas fa-search"></i>Restaurants</a>
                    </li>
                     <li class="nav-item" id="placesTab">
                        <a class="nav-link" id="Places-tab" data-toggle="tab"  href="#Places" role="tab"
                           aria-controls="Discover" aria-selected="false">
                            <i class="fas fa-search"></i>Places</a>
                    </li>


                </ul>
              <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="Vacation" role="tabpanel" aria-labelledby="Vacation-tab">
                        <form action="{{url('activities/search')}}" method="get" id="form_search_act" >
                            <div class="row">
                                <div class="form-group col-md-6">
                                  <!--   <input type="text" class="destination form-control" id="autocomplete" url="{{url('getSearch')}}" placeholder="Destination, Place name or Address"> -->
                                 <!--      <input type="text" class="destination form-control" id="autocomplete" url="{{url('getSearch')}}" placeholder="Destination, Place name or Address"> -->
                            
                                <div class="form-group">
                                    <!-- <label>Search Restaurants</label> -->
                                    <input type="text" name="activtiy_term"
                                           value="@if(isset($_GET['activtiy_term']) && $_GET['activtiy_term']!=""){{trim($_GET['activtiy_term'])}} @endif"
                                           class="form-control required input_hotel" id="act_name"
                                           placeholder="City, activity name">

                                    <input name="city_id" id="act_city_id" type="hidden">
                                    <input name="activity_type" type="hidden">
                                    <span class="icon icon-location" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                <div class="form-group col-12" id="activityoptions">
                                   
                                </div>
                            </div>
                           
                                </div>
                                 <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>Food type</label> -->
                                    <select class="ddslick form-control" name="cuisine" id="style-6">
                                        <option value="" selected>Activities</option>

                                        @if(sizeof($activtySubcategory))
                                        @foreach($activtySubcategory as $sub_cat)
                                            <option value="{{$sub_cat->id}}">{{@$sub_cat->cat_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12" id="options">
                                   
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <button type="submit" class="btn hvr-float-shadow view_all">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="Discover" role="tabpanel" aria-labelledby="Discover-tab">
                        <form action="{{url('restaurants/search')}}" method="get" id="form_search_rest">
                        {{--{{csrf_field()}}--}}
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>Search Restaurants</label> -->
                                    <input type="text" name="term"
                                           value="@if(isset($_GET['term']) && $_GET['term']!=""){{trim($_GET['term'])}} @endif"
                                           class="form-control required input_hotel" id="rest_name"
                                           placeholder="City, Restaurant name">

                                    <input name="rest_city_id" type="hidden">
                                    <input name="type" type="hidden">
                                    <span class="icon icon-location" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                <div class="form-group col-12" id="restaurantoptions">
                                   
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>Food type</label> -->
                                    <select class="ddslick form-control" name="cuisine" id="style-6">
                                        <option value=""
                                                
                                                selected>All restaurants
                                        </option>
                                        @if(!empty($sub_category))
                                        @foreach($sub_category as $sub_cat)
                                            <option value="{{$sub_cat->id}}">{{$sub_cat->cat_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <span class="" aria-hidden="true">
                                @if(Session::has('restaurant_search_error'))
                                <p style="color: red;font-size: 14px"> {{ Session::get('restaurant_search_error') }}</p>
                            @endif</span>
                        <hr>
                          <button type="submit" class="btn hvr-float-shadow view_all">Search</button>

                    </form>
                    </div>

                     <div class="tab-pane fade" id="Places" role="tabpanel" aria-labelledby="Places-tab">
                          <form method="get" action="{{url('places/search')}}" id="form_search_places">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>Search Places</label> -->
                                    {{-- <input type="text" class="form-control"  name="activity_search"
                                            placeholder="Type your search terms">--}}
                                    <input name="city_id" id="place_city_id" value="0" type="hidden">
                                    <input name="search_type" id="search_type" value="0" type="hidden">
                                    <input type="text" name="city"
                                           value="@if(isset($_GET['city']) && $_GET['city']!=""){{trim($_GET['city'])}} @endif"
                                           class="form-control required input_hotel" id="plc_name"
                                           placeholder="City, Place name">
                                    <span class="icon icon-location" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                <div class="form-group col-12" id="placeoptions">
                                   
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>Things to do</label> -->
                                    <select class="ddslick form-control" name="subcategory">
                                        <option value="0"
                                                data-imagesrc="{{url('assets/web')}}/img/icons_search/all_tours.png"
                                                selected>All Places
                                        </option>
                                        @if(!empty($subcat_place))
                                        @foreach($subcat_place as $sub_cat)
                                            <option value="{{$sub_cat->id}}"
                                                    data-imagesrc="{{url('public/uploads')}}/{{$sub_cat->cat_image}}">{{$sub_cat->cat_name}}</option>
                                        @endforeach
                                        @endif
                                       
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <span class="" aria-hidden="true">
                                @if(Session::has('place_search_error'))
                                <p style="color: red;font-size: 14px"> {{ Session::get('place_search_error') }}</p>
                            @endif</span>
                        <hr>
                        <button type="submit" class="btn hvr-float-shadow view_all">Search</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_content">
            <div class="container">
                <div class="row">
              
                    <div class="grid_section explore_iceland text-center">
                 @if(!empty($ActivityData))

                        <h2>
                            <span>More Record</span>
                        </h2>
                  <div class="grid_content row" id="activityAppend">
                       <!--  <div class="gridContentRowDiv">
                             
                        </div> -->
                         @foreach ($ActivityData as $values)
                                <?php //dd($values);?>
                            <div class="col-lg-3 col-md-4 col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                                
                                    <a href="{{url('activity/detail/'.$values['slug'])}}" style="overflow: visible;">
                                    <span class="post_time">{{$values['duration']}}</span>
                                    <div class="d-flex justify-content-center img_wrapper">
                                        @if(isset($values->single_photo)&& !empty($values->single_photo))
                                        <img src="{{ url('public/uploads/activities'.$values->single_photo->photo) }} ">
                                        @else
                                            <img src="{{ url('public/images/no-image.png') }} ">
                                        @endif
                                        <div class="d-flex justify-content-center align-items-center hover_txt">
                                            <p>{{substr($values['description'],0,100)}}
                                                <span>read more...</span>
                                            </p>
                                        </div>
<!--                                        <span>May - Oct</span>-->
                                    </div>
                                    <div class="select_plan">
                                        <span>From:
                                            <strong>{{$values['price']}}</strong>
                                        </span>
                                        <!-- <select class="selectpickerCustom"> -->
                                        <select class="selectpicker">
                                            <option value="USD">USD</option>
                                            <option value="AUD">AUD</option>
                                            <option value="CAD">CAD</option>
                                            <option value="CHF">CHF</option>
                                            <option value="DKK">DKK</option>
                                            <option value="EUR">EUR</option>
                                            <option value="GBP">GBP</option>
                                            <option value="HKD">HKD</option>
                                            <option value="ISK">ISK</option>
                                            <option value="JPY">JPY</option>
                                            <option value="KRW">KRW</option>
                                            <option value="NOK">NOK</option>
                                            <option value="PLN">PLN</option>
                                            <option value="RUB">RUB</option>
                                            <option value="SEK">SEK</option>
                                            <option value="SGD">SGD</option>
                                        </select>
                                    </div>
                                    <span class="place_name">{{$values['activity_name']}}</span>
                                </a>
                            </div>
                            @endforeach
                      <div class="col-12 mt-4 float-left paginateDivMain">
                          <nav aria-label="page navigation example">
                              {{ $ActivityData->links() }}
                          </nav>
                      </div>
                  </div>
                        @endif
                        <span style="display: none" class="showSub">
                             <h2>
                            <span>Sub Categories</span>
                        </h2>
                        <p>Find out all you need to know about Icelandic nature, culture and history
                        </p>
                        <div class="grid_content row" id="catAppenddata">
                           <!--  <div class="col-md-4 col-sm-6">
                                <a href="#">
                                    <div class="d-flex justify-content-center img_wrapper">
                                        <img src="{{url('public/images/landscape24.jpg')}}">
                                        <div class="hover_txt">
                                            <h4>Heading text</h4>
                                            <p>Jump aboard this incredible boat trip in one of the world’s most inspiring places
                                                <span>read more...</span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div> -->
                        </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
     {!! Html::script('assets/admin/js/autocomplete/jquery-ui.js') !!}
    {!! Html::script('assets/web/js/searchAutoComplete.js') !!}
<script type="text/javascript">
$('#autocomplete').autocomplete({
source: 'getSearch',
appendTo: "#options"
});
</script>
<script type="text/javascript">
      $("#act_name").autocomplete({
            source: web_url + "/search/SearchActAutoName",
            appendTo:'#activityoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#act_city_id').val(e.id);
                $('input[name=activity_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#act_name').val() === 'No records found') {
                    $("#act_name").val("");
                    $("#act_name").attr("disabled", false);
                }
            }
        });
      $("#plc_name").autocomplete({
            source: web_url + "/search/SearchPlcAutoName",
            appendTo:'#placeoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('#place_city_id').val(e.id);
                $('input[name=search_type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#plc_name').val() === 'No records found') {
                    $("#plc_name").val("");
                    $("#plc_name").attr("disabled", false);
                }
            }
        });
        $("#rest_name").autocomplete({
            source: web_url + "/search/searchAutoRestName",
            appendTo:'#restaurantoptions',
            select: function (event, ui) {
                var e = ui.item;
                $('input[name=rest_city_id]').val(e.id);
                $('input[name=type]').val(e.type);
            },
            change: function (event, ui) {
                var noRecord = 'No records found';
                if (ui.item == null || ui.item == undefined || $('#rest_name').val() === 'No records found') {
                    $("#rest_name").val("");
                    $("#rest_name").attr("disabled", false);
                }
            }
        });
        
</script>
 <script type="text/javascript">
     $(document).ready(function(){
          $(document).on('click','#Vacation-tab', function(){
            // $('#gridContentRowDiv').hide();
        // $('#preloader').show();
         location.reload(true);
  /*$.ajax
    ({ 
        type: 'get',
        url: web_url+'/activities-more',
        success: function(result)
        {
           $('#activityAppend').html(result);
             $('#preloader').hide();
        }
    });*/
        });
        // /////////////////////////////// places click ///////////////////////////
        $(document).on('click','#placesTab', function(){
        $('#preloader').show();
            $('.fadeInLeft').hide();
            $('.paginateDivMain').hide();
            $('.showSub').show();
  $.ajax
    ({ 
        type: 'get',
        url: web_url+'/place/subcategory',
        success: function(result)
        {
           $('#catAppenddata').html(result);
             $('#preloader').hide();
        }
    });
        });
         $(document).on('click','#restaurantTab', function(){
             $('#preloader').show();
                 $('.fadeInLeft').hide();
                 $('.paginateDivMain').hide();
                  $('.showSub').show();
  $.ajax
    ({ 
        type: 'get',
        url: web_url+'/restaurant/subcategory',
        success: function(result)
        {

           $('#catAppenddata').html(result);
            $('#preloader').hide();
        }
    });
        });
     })
 </script>

  {{--@include('layouts.jquery_pages.homeScript'); --}}
@endsection
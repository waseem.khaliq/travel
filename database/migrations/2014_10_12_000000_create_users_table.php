<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
                Schema::create('users', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('first_name', 100)->nullable();
                    $table->string('last_name', 100)->nullable();
                    $table->string('name', 100)->nullable();
                    $table->string('email')->unique();
                    $table->string('password');
                    $table->string('phone_no', 20)->nullable();
                    $table->string('user_photo')->nullable();
                    $table->string('address')->nullable();
                    $table->string('city', '50')->nullable();
                    $table->string('state', '50')->nullable();
                    $table->string('country_code',2)->nullable();
                    $table->enum('user_type', ['super-admin', 'staff','admin', 'user', 'hotel', 'restaurant', 'car'])->default('user');
                    $table->enum('status', ['Active', 'Inactive'])->default('Inactive');
                    $table->integer('created_by')->nullable();
                    $table->string('api_token',60)->nullable();
                    $table->rememberToken();
                    $table->string('facebook_id', '100')->nullable();
                    $table->string('google_id', '100')->nullable();
                    $table->string('twitter_id', '100')->nullable();
                    $table->integer('external_user_id')->nullable();
                    $table->timestamps();
                    $table->softDeletes();

                });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}

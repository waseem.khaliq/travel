<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
class User extends Model implements Authenticatable
{
    protected $table = "users";
    protected $guarded = ['id','created_at', 'updated_at'];
    protected $fillable = ['first_name','last_name','email','password','address','city','country_code','zip','api_token'];
        public function getAuthIdentifierName(){
        	
        }
    public function getAuthIdentifier(){
    	$v = "";
    	return null;
    }
    public function getAuthPassword(){
    	return null;
    }
    public function getRememberToken(){
    	return null;
    }
    public function setRememberToken($value){
    	return null;
    }
    public function getRememberTokenName(){
    	return null;
    }

}

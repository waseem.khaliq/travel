<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Api\ApiLoginController@auth');
Route::post('register', 'Api\ApiLoginController@register');
Route::get('places', 'Api\ApiplaceController@places');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('users', 'Api\ApiLoginController@user_listing');
});

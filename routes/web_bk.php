<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::get('/', function () {
    // return redirect('/login');
     return view('welcome');
});*/

Route::get('/', 'HomeController@index');

Route::get('login', 'LoginController@login');
Route::post('signup', 'AdLoginController@signup');
Route::post('logout', 'AdLoginController@logout');

use App\User;
Auth::routes();

 Route::get('admin', 'Admin\AdLoginController@index');
Route::prefix('admin')->group(function () {
    Route::get('signup', 'Admin\AdLoginController@signup');
    Route::post('logout', 'Admin\AdLoginController@logout');
    // Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::get( 'ausers/api', 'Admin\UserController@apiUsers')->name('api.users');
     Route::get( 'users-lisitng', 'Admin\UserController@users_lisitng')->name('users-lisitng');
    Route::resource( 'users', 'Admin\UserController');
    Route::get( 'administrator-listing', 'Admin\AdminController@administrator_listing')->name('administratorListing');
    Route::resource( 'administrator', 'Admin\AdminController');

    Route::get( 'placesListing', 'Admin\PlacesController@placesListing' )->name('placesListing');

    Route::get( 'places/remove_image', 'Admin\PlacesController@remove_image' );

    Route::get( 'places-listing', 'Admin\PlacesController@places_listing' )->name('places_listing'); 
    
Route::get( 'places/remove_image', 'Admin\PlacesController@remove_image' ); 
Route::get( 'places/import-excel', 'Admin\PlacesController@import_excel' ); 
Route::post('places/import-places', 'Admin\PlacesController@store_excel');
Route::get('places/import-rcg-plc' , 'Admin\PlacesController@import_rcg_plc');
Route::get('places/import-idiscover-plc' , 'Admin\PlacesController@import_idiscover_plc');
Route::resource( '/places', 'Admin\PlacesController' );

    Route::get( 'activities/api', 'Admin\ActivityController@api' );

    Route::resource( '/activities', 'Admin\ActivityController' );
    // articals ////////////////////
   Route::get( 'articles/remove-image/{id}', 'Admin\ArticlesController@remove_image' ); 
    Route::get( 'articles-listing ', 'Admin\ArticlesController@articles_listing' )->name('articles_listing');
    Route::resource( '/articles', 'Admin\ArticlesController' );

    ////////////// restaurants ///////////////////////////////////////////////////////////
 Route::get('restaurants/import-rcg-places', 'Admin\RestaurantController@import_rcg_rst');
     Route::get('restaurants/import-idiscover-rst', 'Admin\RestaurantController@import_idiscover_rst');
Route::get( '/restaurants/address_delet/{id}', 'Admin\RestaurantController@address_delet'); 
Route::get( '/restaurants/address/{id}', 'Admin\RestaurantController@address'); 
Route::post( '/restaurants/address_store/{id}', 'Admin\RestaurantController@address_store'); 
Route::get( '/restaurants/remove_image/{id}', 'Admin\RestaurantController@remove_image' ); 
Route::post( '/restaurants/edit_address', 'Admin\RestaurantController@edit_address');
Route::get( '/restaurant-listing', 'Admin\RestaurantController@restaurant_listing'); 
Route::get( '/restaurants/address/{id}', 'Admin\RestaurantController@address' ); 
Route::resource( '/restaurants', 'Admin\RestaurantController' );


    Route::get( 'categoriesListing', 'Admin\CategoriesController@categoryListing' )->name('categoryListing');
    Route::resource( '/categories', 'Admin\CategoriesController' );

    Route::get( 'subcategories/subcategoriesListing', 'Admin\SubCategoriesController@subcategoriesListing' )->name('subcategoriesListing');
    Route::resource( '/subcategories', 'Admin\SubCategoriesController' );
    ////////////// keyword ///////////////
    Route::get( 'keyword-listing', 'Admin\KeywordsController@keyword_listing' )->name('keyword-listing');
    Route::resource( 'keywords', 'Admin\KeywordsController' );
    ////////////// Delas ///////////////
    Route::get( '/deals/get_places/{id}', 'Admin\DealsController@get_places' );

    Route::get( 'dealsListing', 'Admin\DealsController@dealsListing' )->name('dealsListing');
    Route::resource( 'deals', 'Admin\DealsController' );
    ////////////// setting ///////////////
    Route::post( 'update_setting', 'Admin\SettingController@update_setting' );
    Route::resource( 'setting', 'Admin\SettingController' );
    ////////////// profile ///////////////
    Route::post( 'profile/update_image', 'Admin\ProfileController@update_image' );
    Route::post( 'profile/change_password', 'Admin\ProfileController@change_password' );
    Route::post( 'profile/update', 'Admin\ProfileController@update_profile' );
    Route::resource( 'profile', 'Admin\ProfileController' );



     ////////////// permission ///////////////
    Route::get( 'permission/api', 'Admin\PermissionController@apiPermission' )->name('api.permission');
    Route::resource( 'permission', 'Admin\PermissionController' );
    ////////////// role ///////////////
    Route::get( 'role/api', 'Admin\RoleController@apirole' )->name('api.role');
    Route::resource( 'role', 'Admin\RoleController' );

////////////// EmailTemplatesController ///////////////
     Route::get( '/email-templates-listing', 'Admin\EmailTemplatesController@email_templates_listing' );
    Route::resource( 'email-templates', 'Admin\EmailTemplatesController');


    });
    Route::group(['middleware' => ['web', 'auth']], function () {
    Route::post( 'common/status', 'CommonController@update_status' )->name('status.all');
    Route::post( 'common/delete', 'CommonController@delete' )->name('delete.all');
    Route::post( 'common/bulk-oprations', 'CommonController@bulkOprations' )->name('bulk-oprations.all');


    //Route::get( 'users/{user}/getData', 'UserController@getData' )->name('users.getData');
  
    ////////////// order ///////////////
    Route::get( 'order/api', 'OrderController@apiOrder' )->name('api.order');
    Route::resource( 'orders', 'OrderController' );
});


    Route::get('/datatables_custom', 'DatatablesCustomController@index');
    Route::get('/form-controls', 'DatatablesCustomController@form');
    Route::get('/multi-column-forms', 'DatatablesCustomController@multicolumnforms');

    ///////////Order Controller ///////////
    Route::get('/order_listing', 'OrderController@index');
